<?php
/*
Template Name: Contact
*/

get_header(); ?>
<section id="coordonates">
    <div class="container">
        <div class="box">
            <div class="wrapper-coordonates">
                <div class="title-wrapper">
                    <h2 class="title title--ctr"><?php the_field('info_title'); ?></h2>
                </div>
                <div class="generic-box row around-md">
                    <?php

                    if (have_rows('coordonates')) :

                        // loop through the rows of data
                        while (have_rows('coordonates')) : the_row(); ?>

                            <div class="item">
                                <h3><?php the_sub_field('coordonates_title'); ?></h3>
                                <p><?php the_sub_field('coordonates_content'); ?></p>
                            </div>
                    <?php endwhile;
                    endif;
                    ?>
                </div>
            </div>
            <section id="map">
                <?php echo do_shortcode('[leaflet-map lat="50.53480248270557" lng="4.602871841298587" zoomcontrol]'); ?>
                <?php echo do_shortcode('[leaflet-marker lat="50.534336228049696" lng="4.602419183923049"]'); ?>
            
            </section>
        </div>
    </div>

</section>

<?php get_footer(); ?>