<?php

/**
 * The template for displaying search results pages
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 */

get_header(); ?>

<h2 class="title__section title__page"><?php _e('Search Results for:', 'wpand-theme'); ?> <?php echo esc_attr(get_search_query()); ?></h2>

<!-- search section -->
<section id="learning-courses">
	<div id="courses-delivered" class="no-swiper">
		<div class="container">
			<div class="learning-courses__wrapper  content__section--small content__wrapper">
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						<a class="courses-delivered__single" href="<?php echo the_permalink(); ?>">
							<div>
								<div class="course__single--title">
										<?php if (get_the_post_thumbnail()) : ?>
											<?php the_post_thumbnail('full'); ?>
										<?php else : ?>
											<img src="<?php echo get_template_directory_uri(); ?>/assets/images/default-learning.jpg" alt="">
										<?php endif; ?>
								</div>
								<div class="course__single--content">
									<h3><?php echo get_the_title(); ?></h3>
									<?php
									$topics = get_the_terms(get_the_ID(), 'topic');
									foreach ($topics as $term) { ?>
										<span class="flag"> #<?php echo $term->name; ?></span>
									<?php } ?>
								</div>
								<div class="read-more">

									<?php _e('Learn more', 'wpand-theme'); ?>
								</div>
							</div>
						</a>
					<?php endwhile; ?>
					<?php joints_page_navi(); ?>

				<?php else : ?>
					<p>No articles found</p>
				<?php endif; ?>
			</div>
		</div>
		<!-- search section -->
		<section id="search" class="widget_search">
			<div class="container">
				<h2 class="title__section inverted white"><?php _e('Search with others terms', 'wpand-theme'); ?></h2>

				<?php if (get_field('homepage_search_subtitle')) : ?>
					<h3 class="subtitle__section inverted"><?php the_field('homepage_search_subtitle'); ?></h3>
				<?php endif; ?>

				<?php get_search_form(); // echo form from searchform.php
				?>
			</div>
		</section>
		<!-- end search section -->
	</div>
</section>

<?php get_footer(); ?>