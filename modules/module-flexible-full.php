<?php

// Check value exists.
if (have_rows('modules')) : ?>
<div class="modules">
    <?php while (have_rows('modules')) : the_row();

            // Case: Module CTA
            if (get_row_layout() == 'module_cta') : ?>
    <div class="mdl mdl-cta">
        <div class="content__section" data-aos="fade-in" data-aos-delay="400" data-aos-duration="1000">
            <div class="cta">
                <div class="container">
                    <div class="wrapper col-xs-12 col-md-offset-4 col-md-5">
                        <div class="title-area title__span">
                            <h2><?php the_sub_field('cta_title'); ?></h2>
                            <span class="span"><?php the_sub_field('cta_span'); ?></span>
                        </div>
                        <p><?php the_sub_field('cta_description'); ?></p>
                        <?php
                        $link = get_sub_field('cta_btn');
                        if (!empty($link)) : ?>
                        <a class='button primary' href='<?php echo $link['url']; ?>'
                            target='<?php $link['target']; ?>'><?php echo $link['title']; ?></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
            // Case: Module Quote
            elseif (get_row_layout() == 'module_quote') : ?>
    <div class="mdl mdl-quote">
        <div class="container" data-aos="fade-right" data-aos-delay="200" data-aos-duration="1000">
            <div class="content__section">
                <div class="quote">
                    <h2 class="title--ctr"><?php the_sub_field('quote'); ?></h2>
                </div>
            </div>
        </div>
    </div>
    <?php

            // Case: Module Image Single
            elseif (get_row_layout() == 'module_image') : ?>
    <div class="mdl mdl-img">
        <div class="content__section">
            <div class="container">
                <?php $image = get_sub_field('image_single');
            if( $image ) {
                echo wp_get_attachment_image( $image, 'full' );
            }
            ?>
            </div>
        </div>
    </div>
    <?php
        // Case: Module Title Text 
        elseif (get_row_layout() == 'module_tt') : ?>
    <div class="mdl mdl-tt" style="background-color: <?php the_sub_field('module_tt_bg_color'); ?>">
        <div class="content__section">
            <div class="container">
                <div class="row">
                    <div class="title-area col-xs-12 col-md-5 title__span" data-aos="fade-left" data-aos-delay="200"
                        data-aos-duration="1000">
                        <h2><?php the_sub_field('module_tt_title'); ?></h2>
                        <span class="span"><?php the_sub_field('module_tt_span'); ?></span>
                    </div>
                    <div class="text-area col-xs-12 col-md-5 col-md-offset-1" data-aos="fade-right" data-aos-delay="200"
                        data-aos-duration="1000">
                        <?php the_sub_field('module_tt_content'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
            // Case: Module Text Single
            elseif (get_row_layout() == 'module_text_single') : ?>
    <div class="mdl mdl-text <?php echo (get_sub_field('text_size')) ? 'full-text' : ''; ?>"
        style="background: <?php the_sub_field('text_single_bg_color'); ?>">
        <div class="container" data-aos="fade-up" data-aos-delay="200" data-aos-duration="1000">
            <div class="content__section">
                <div
                    class="text-single <?php echo (get_sub_field('text_single_col_count') == "col_2") ? 'col_2' : ''; ?>">
                    <?php the_sub_field('text_single'); ?>
                </div>
            </div>
        </div>
    </div>
    <?php
            // Case: Module text-image
            elseif (get_row_layout() == 'module_text_image') : ?>
    <div class="mdl mdl-txt-img">
        <div class="container">
            <div class="content__section">
                <div class="wrapper align-<?php the_sub_field('alignment'); ?>">
                    <div class="image">
                        <div class="img-wrapper">
                            <?php $image = get_sub_field('image');
                            if( $image ) {
                            echo wp_get_attachment_image( $image, 'full' );
                            }
                        ?>
                        </div>
                    </div>
                    <div class="content" data-aos="fade-up" data-aos-delay="200" data-aos-duration="1000">
                        <?php the_sub_field('text'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
            // Case: Module Gallery
            elseif (get_row_layout() == 'module_gallery') : ?>
    <div class="mdl mdl-gallery">
        <div class="content__section container">
            <div class="title__span col-xs-12 col-md-4 col-md-offset-2" data-aos="fade-right" data-aos-delay="200"
                data-aos-duration="1000">
                <h2><?php the_sub_field('gallery_title'); ?></h2>
                <span class="span"><?php the_sub_field('gallery_span'); ?></span>
            </div>
            <div class="gallery" data-aos="fade-up" data-aos-delay="200" data-aos-duration="1000">
                <div id="swiper-gallery" class="swiper-container swiper-gallery">
                    <div class="swiper-wrapper">

                        <?php
                                    $images = get_sub_field('gallery');
                                    $size = 'full'; // (thumbnail, medium, large, full or custom size)
                                    if ($images) : ?>
                        <?php foreach ($images as $image_id) : ?>
                        <div class="swiper-slide col-xs-12 col-md-4">
                            <div class="swiper-img-content">
                                <a href="<?php echo wp_get_attachment_url($image_id, $size); ?>"
                                    data-lightbox='gallery'>
                                    <img src="<?php echo wp_get_attachment_url($image_id, $size); ?>" alt=""></a>
                            </div>
                        </div>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </div>
    <?php

    // Case: Module Masonry
    elseif (get_row_layout() == 'module_masonry') : ?>
    <div class="mdl mdl-masonry">
        <div class="content__section container">
            <div class="title__span col-xs-12 col-md-4 col-md-offset-2" data-aos="fade-right" data-aos-delay="200"
                data-aos-duration="1000">
                <h2><?php the_sub_field('masonry_title'); ?></h2>
                <span class="span"><?php the_sub_field('masonry_span'); ?></span>
            </div>
            <div class="masonry col-xs-12" data-aos="fade-up" data-aos-delay="200" data-aos-duration="1000">
                <div class="grid">
                    <?php
                        $images = get_sub_field('masonry');
                        $size = 'full'; // (thumbnail, medium, large, full or custom size)
                        if ($images) : ?>
                    <?php foreach ($images as $image_id) : ?>

                    <div class="grid-item">
                        <div class="swiper-img-content">
                            <a href="<?php echo wp_get_attachment_url($image_id, $size); ?>" data-lightbox='masonry'>
                                <img src="<?php echo wp_get_attachment_url($image_id, $size); ?>" alt=""></a>
                        </div>
                    </div>

                    <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <?php
            // Case: Module Accordeon
            elseif (get_row_layout() == 'module_accordeon') : ?>
    <div class="mdl mdl-accordeon">
        <div class="container col-xs-12 col-md-6 col-md-offset-3" data-aos="fade-in" data-aos-delay="400"
            data-aos-duration="1000">
            <div class="content__section">
                <ul class="no-list accordeon">
                    <h2><?php the_sub_field('accordeon_title'); ?></h2>

                    <?php
                                if (have_rows('accordeon')) :
                                    while (have_rows('accordeon')) : the_row(); ?>
                    <li class="accordeon__item">
                        <div class="content col-lg-12">
                            <div class="accordeon__item--title">
                                <h3><?php the_sub_field('title_item'); ?></h3>
                                <span class="expand">
                                    <span class="line line1"></span>
                                    <span class="line line2"></span>
                                </span>
                            </div>
                            <div class="accordeon__item--content">
                                <p><?php the_sub_field('content_item'); ?></p>
                            </div>
                        </div>
                    </li>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
    <?php

            // Case: Module PDF file
            elseif (get_row_layout() == 'module_pdf') : ?>
    <div class="mdl mdl-file">
        <div class="module-pdf">
            <div class="container">
                <div class="content__section">
                    <?php
                                if (have_rows('pdf_list')) :
                                    while (have_rows('pdf_list')) : the_row(); ?>
                    <div class="item row between-md middle-md" data-aos="fade-left" data-aos-delay="200"
                        data-aos-duration="1000">
                        <div class="col-md-6">
                            <h3 class="pdf-h3"><?php the_sub_field('pdf_title'); ?></h3>
                            <?php if (get_sub_field('pdf_description')) : ?>
                            <p><?php the_sub_field('pdf_description'); ?></p>
                            <?php endif; ?>
                            <?php
                                                $file = get_sub_field('pdf_file');
                                                if ($file) : ?>
                        </div>
                        <div class="col-md-6 center-md">
                            <a class="link-pdf" href="<?php echo $file['url']; ?>" target="_blank">
                                <?php if (get_sub_field('pdf_title')) : ?>
                                <?php _e('Télécharger', 'Beokis'); ?>
                                <?php endif; ?>
                            </a>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php endwhile;
                                endif; ?>
                </div>
            </div>
        </div>
    </div>
    <?php

    // Case: Module Comparateur

    elseif (get_row_layout() == 'module_comparator') : ?>
    <div class="mdl mdl-comparator">
        <div class="module-comparator">
            <div class="container">
                <div class="content__section">
                    <div class="title-area col-xs-12 col-md-4 col-md-offset-2 title__span" data-aos="fade-left"
                        data-aos-delay="200" data-aos-duration="1000">
                        <h2><?php the_sub_field('comparator_title'); ?></h2>
                        <span class="span"><?php the_sub_field('comparator_span'); ?></span>
                    </div>
                    <div>
                        <?php if (have_rows('comparator_img')) :
                                    while (have_rows('comparator_img')) : the_row(); ?>
                        <?php $number = get_row_index(); ?>
                        <?php endwhile; ?>
                        <?php endif; ?>
                        <?php if($number > 1): ?>
                        <div id="swiper-comparator" class="swiper-container swiper-comparator">
                            <div class="swiper-wrapper">
                                <?php endif; ?>
                                <?php
                                    if (have_rows('comparator_img')) :
                                    while (have_rows('comparator_img')) : the_row(); ?>
                                <?php if($number > 1): ?>
                                <div class="swiper-slide">
                                    <?php endif; ?>
                                    <div>
                                        <div class='twentytwenty-container'>
                                            <img src="<?php the_sub_field('comparator_img_before'); ?>" />
                                            <img src="<?php the_sub_field('comparator_img_after'); ?>" />
                                        </div>
                                        <?php if($number > 1): ?>
                                    </div>
                                    <?php endif; ?>
                                    <div class="comparator_content">
                                        <?php the_sub_field('comparator_description'); ?>

                                    </div>
                                </div>

                                <?php endwhile;
                                    endif; ?>
                                <?php if($number > 1): ?>
                            </div>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    // Case: Module Team
    elseif (get_row_layout() == 'module_team') : ?>
    <div class="mdl mdl-team">
        <div class="container" data-aos="fade-up" data-aos-delay="200" data-aos-duration="1000">
            <div class="content__section">
                <div class="title-area col-xs-12 col-md-4 col-md-offset-2 title__span">
                    <h2><?php the_sub_field('module_team_title'); ?></h2>
                    <span class="span"><?php the_sub_field('module_team_span'); ?></span>
                </div>
                <section id="team">
                    <?php get_template_part('parts/page', 'team-member'); ?>
                </section>
            </div>
        </div>
    </div>
    <?php
    // Case: Module Blog
    elseif (get_row_layout() == 'module_blog') : ?>
    <div class="mdl mdl-blog">
        <div class="container" data-aos="fade-up" data-aos-delay="200" data-aos-duration="1000">
            <div class="content__section">
                <div class="title-area col-xs-12 col-md-4 col-md-offset-2 title__span">
                    <h2><?php the_sub_field('module_blog_title'); ?></h2>
                    <span class="span"><?php the_sub_field('module_blog_span'); ?></span>
                </div>
                <section id="blog">
                    <?php get_template_part('parts/page', 'blog'); ?>
                </section>
            </div>
        </div>
    </div>
    <?php

        // Case: Module Review
    elseif (get_row_layout() == 'module_review') : ?>
    <div class="mdl mdl-review">
        <div class="container" data-aos="fade-up" data-aos-delay="200" data-aos-duration="1000">
            <div class="content__section">
                <div class="title-area col-xs-12 col-md-5 col-md-offset-2 title__span">
                    <h2><?php the_sub_field('module_review_title'); ?></h2>
                    <span class="span"><?php the_sub_field('module_review_span'); ?></span>
                </div>
                <section id="review">
                    <?php get_template_part('parts/page', 'review'); ?>
                </section>
            </div>
        </div>
    </div>
    <?php

                // Case: Module Product
    elseif (get_row_layout() == 'module_product') : ?>
    <div class="mdl mdl-product">
        <div class="container" data-aos="fade-up" data-aos-delay="200" data-aos-duration="1000">
            <div class="content__section">
                <div class="title-area col-xs-12 col-md-5 col-md-offset-2 title__span">
                    <h2><?php the_sub_field('module_product_title'); ?></h2>
                    <span class="span"><?php the_sub_field('module_product_span'); ?></span>
                </div>
                <section id="products">
                    <?php get_template_part('parts/page', 'products'); ?>
                </section>
            </div>
        </div>
    </div>
    <?php

                // Case: Module Events
    elseif (get_row_layout() == 'module_event') : ?>
    <div class="mdl mdl-event">
        <div class="container" data-aos="fade-up" data-aos-delay="200" data-aos-duration="1000">
            <div class="content__section">
                <div class="title-area col-xs-12 col-md-5 col-md-offset-2 title__span">
                    <h2><?php the_sub_field('module_event_title'); ?></h2>
                    <span class="span"><?php the_sub_field('module_event_span'); ?></span>
                </div>
                <section id="event">
                    <?php get_template_part('parts/page', 'events'); ?>
                </section>
            </div>
        </div>
    </div>
    <?php
                
            // Case: Module Table
            elseif (get_row_layout() == 'module_table') : ?>
    <div class="mdl mdl-table">
        <div class="container">
            <div class="content__section">
                <div class="title-area title__span col-xs-12 col-md-4 col-md-offset-1" data-aos="fade-right"
                    data-aos-delay="200" data-aos-duration="1000">
                    <h2><?php the_sub_field('table_title'); ?></h2>
                    <span class="span"><?php the_sub_field('table_span'); ?></span>
                </div>
                <?php

                            $table = get_sub_field('table');

                            if (!empty($table)) {

                                echo '<table class="table" border="0" class="col-xs-12 col-md-8 col-md-offset-2" data-aos="fade-up" data-aos-delay="200" data-aos-duration="1000">';

                                if (!empty($table['header'])) {

                                    echo '<thead>';

                                    echo '<tr>';

                                    foreach ($table['header'] as $th) {

                                        echo '<th>';
                                        echo $th['c'];
                                        echo '</th>';
                                    }

                                    echo '</tr>';

                                    echo '</thead>';
                                }

                                echo '<tbody>';

                                foreach ($table['body'] as $tr) {

                                    echo '<tr>';

                                    foreach ($tr as $td) {



                                        echo '<td>';
                                        if ($td['c'] == "V") {
                                            echo '<span class="check"></span>';
                                        } elseif ($td['c'] == "X") {
                                            echo '<span class="cross"></span>';
                                        } else {
                                            echo $td['c'];
                                        }
                                        echo '</td>';
                                    }

                                    echo '</tr>';
                                }

                                echo '</tbody>';

                                echo '</table>';
                            }
                            ?>
            </div>
        </div>
    </div>

    <?php endif;
        endwhile; ?>
</div>
<?php else :
// Do something...
endif;

?>