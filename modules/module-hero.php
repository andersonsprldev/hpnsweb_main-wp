<!-- Here you can find hero for homepage and for others page to customize them as needed -->


<!-- Hero HOMEPAGE -->

<?php if (is_front_page()) : ?>

    <section id="hero" class="hero__homepage" data-aos="fade-down" data-aos-duration="1000" style="background-image: url('<?php the_field('hero_image'); ?>')">
        <div class="hero__wrapper container">
            <div class="row">
                <div class="col-md-12 hero__content" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">
                    <h1 class="title__page">
                        <span class="white"><?php the_field('hero_title'); ?></span>
                        <span class="small subtitle"><?php the_field('hero_subtitle'); ?></span>
                    </h1>
                    <?php if (get_field('hero_description')) : ?>
                        <h2 class="subtitle__page"><?php the_field('hero_description'); ?></h2>
                    <?php endif; ?>
                    <div class="cta-group">
                        <?php
                        $link = get_field('hero_cta');
                        if ($link) :
                        ?>
                            <a class="button primary parents" href="<?php echo $link['url']; ?>" target="<?php $link['target']; ?>"><?php echo $link['title']; ?></a>
                        <?php endif; ?>
                        <?php
                        $link2 = get_field('hero_cta_2');
                        if ($link2) :
                        ?>
                            <a class="button secondary pro" href="<?php echo $link2['url']; ?>" target="<?php $link2['target']; ?>"><?php echo $link2['title']; ?></a>
                        <?php endif; ?>
                    </div>
                </div>

                <?php if (get_field('hero_img')) :
                    $image = get_field('hero_img'); ?>
                    <?php echo wp_get_attachment_image($image, 'full'); ?>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php elseif (is_page_template('template-landing.php')) : ?>
    <?php if (get_field('tool_side_button')) : ?>
        <a class='button side doc' href='<?php the_field('tool_url'); ?>' target="_blank"><i class="far fa-file-alt"></i> <?php the_field('tool_text'); ?></a>
    <?php endif; ?>
    <section id="hero" class="hero hero-landing" data-aos="fade-down" data-aos-duration="1000">
        <div class="banner" style="background-image: url('<?php the_field('hero_banner'); ?>')"></div>
        <div class="hero__wrapper container">
            <div class="row">
                <div class="hero__content" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">
                    <h1 class="title__page">
                        <span class="small-title"><?php the_field('hero_subtitle_top'); ?></span>
                        <?php the_field('hero_title'); ?>
                    </h1>
                    <div class="row between-md">
                        <div class="col-md-7">
                            <?php if (get_field('hero_description')) : ?>
                                <h2 class="subtitle__page"><?php the_field('hero_description'); ?></h2>
                            <?php endif; ?>
                            <?php
                            $link = get_field('hero_cta');
                            if ($link) :
                            ?>
                                <a class="button secondary" href="#section3"><?php echo $link['title']; ?></a>
                            <?php endif; ?>
                        </div>
                        <div class="prices col-xs-12 col-md-4">
                            <div class="box__infos box-hours">
                                <span class="number"><?php the_field('duration_formation'); ?></span>
                                <span class="desc">Heures de formation</span>
                            </div>
                            <div class="box__infos box-price">
                                <?php if (get_field('promotion_formation')) : ?>
                                    <span class="initial-price"><?php the_field('price_formation'); ?>€</span>
                                    <span class="price"><?php the_field('promotion_price_formation'); ?>€</span>
                                    <span class="desc">offre valable jusqu'au <?php the_field('end_date_promotion'); ?></span>
                                <?php else : ?>
                                    <span class="price"><?php the_field('price_formation'); ?>€</span>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Hero Lead Magnet  -->
<?php elseif (is_page_template('template-lead-magnet.php')) : ?>
    <section id="hero" class="hero hero-lead-magnet" data-aos="fade-down" data-aos-duration="1000">
        <div class="banner" style="background-image: url('<?php the_field('hero_image'); ?>')"></div>
        <div class="hero__wrapper container">
            <div class="row">
                <div class="hero__content" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">
                    <h1 class="title__page">
                        <span class="small-title"><?php the_field('hero_subtitle_top'); ?></span>
                        <?php the_field('hero_title'); ?>
                    </h1>
                    <div class="row between-md">
                        <div class="col-md-7">
                            <?php if (get_field('hero_subtitle')) : ?>
                                <h2 class="subtitle__page"><?php the_field('hero_subtitle'); ?></h2>
                            <?php endif; ?>
                            <a id="button-ga" class="button secondary openModal button-ga" href=""><i class="far fa-file-alt"></i> Télécharger l'outil</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

<?php else : ?>

    <!-- Hero OTHERS PAGES -->

    <section id="hero" class="hero" data-aos="fade-down" data-aos-duration="1000" style="background-image: url('<?php the_field('hero_image'); ?>')">
        <div class="hero__wrapper container">
            <div class="row">
                <div class="hero__content col-lg-9" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="300">
                    <h1 class="title__page"><?php the_field('hero_title'); ?></h1>
                    <?php if (get_field('hero_subtitle')) : ?>
                        <h2 class="subtitle__page"><?php the_field('hero_subtitle'); ?></h2>
                    <?php endif; ?>
                    <?php
                    $link = get_field('hero_cta');
                    if ($link) :
                    ?>
                        <a class="button primary" href="<?php echo $link['url']; ?>" target="<?php $link['target']; ?>"><?php echo $link['title']; ?></a>
                    <?php endif; ?>
                </div>
                <div class="hero__illu">
                    <?php if (get_field('hero_image')) :
                        $image = get_field('hero_image'); ?>
                        <?php echo wp_get_attachment_image($image, 'full'); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

<?php endif; ?>