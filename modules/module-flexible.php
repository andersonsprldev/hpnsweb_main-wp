<?php

// Check value exists.
if (have_rows('modules')) :

    // Loop through rows.
    while (have_rows('modules')) : the_row();

        // Case: Module CTA
        if (get_row_layout() == 'module_cta') :
?>
            <div class="content__section">
                <div class="cta">
                    <div class="container">
                        <div class="wrapper">
                            <h2 class="title__cta center"><?php the_sub_field('title'); ?></h2>
                            <p><?php the_sub_field('description'); ?></p>
                            <?php
                            $link = get_sub_field('button');
                            if (!empty($link)) : ?>
                                <a class="button primary" href="<?php echo $link['url']; ?>" target="<?php $link['target']; ?>"><?php echo $link['title']; ?></a>
                            <?php endif; ?>
                            <?php
                            $link2 = get_sub_field('button_2');
                            if (!empty($link2)) : ?>
                                <a class="button secondary" href="<?php echo $link2['url']; ?>" target="<?php $link2['target']; ?>"><?php echo $link2['title']; ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php

        // Case: Module text-image
        elseif (get_row_layout() == 'module_text_image') : ?>
            <div class="content__section">
                <div class="container">

                    <div class="text-image align-<?php the_sub_field('alignment'); ?>">
                        <div class="img-wrapper">
                            <?php
                            $image = get_sub_field('image');
                            if (!empty($image)) : ?>
                                <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                            <?php endif; ?>
                        </div>
                        <div class="content-wrapper">
                            <?php the_sub_field('text'); ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        // Case: Module Gallery
        elseif (get_row_layout() == 'module_gallery') : ?>
            <div class="content__section">
                <div class="gallery">
                    <div class="swiper-container swiper-gallery">
                        <div class="swiper-wrapper">
                            <?php
                            $images = get_sub_field('gallery');
                            $size = 'full'; // (thumbnail, medium, large, full or custom size)
                            if ($images) : ?>
                                <?php foreach ($images as $image_id) : ?>
                                    <div class="swiper-slide">
                                        <div class="img-wrapper">
                                            <?php echo wp_get_attachment_image($image_id, $size); ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php
        // Case: Module Accordeon
        elseif (get_row_layout() == 'module_accordeon') : ?>
            <div class="container">
                <div class="content__section">
                    <ul class="no-list accordeon">
                        <h2 class="title"><?php the_sub_field('accordeon_title'); ?></h2>

                        <?php
                        if (have_rows('accordeon')) :
                            while (have_rows('accordeon')) : the_row(); ?>
                                <li class="accordeon__item box">
                                    <div class="content col-lg-12">
                                        <div class="accordeon__item--title">
                                            <h3><?php the_sub_field('title_item'); ?></h3>
                                            <span class="expand">
                                                <span class="line line1"></span>
                                                <span class="line line2"></span>
                                            </span>
                                        </div>
                                        <div class="accordeon__item--content">
                                            <p><?php the_sub_field('content_item'); ?></p>
                                        </div>
                                    </div>
                                </li>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>

        <?php
        // Case: Module Table
        elseif (get_row_layout() == 'module_table') : ?>
            <div class="container">
                <div class="content__section">
                    <h2><?php the_sub_field('table_title'); ?></h2>
                    <?php

                    $table = get_sub_field('table');

                    if (!empty($table)) {

                        echo '<table border="0">';

                        if (!empty($table['header'])) {

                            echo '<thead>';

                            echo '<tr>';

                            foreach ($table['header'] as $th) {

                                echo '<th>';
                                echo $th['c'];
                                echo '</th>';
                            }

                            echo '</tr>';

                            echo '</thead>';
                        }

                        echo '<tbody>';

                        foreach ($table['body'] as $tr) {

                            echo '<tr>';

                            foreach ($tr as $td) {



                                echo '<td>';
                                if ($td['c'] == "V") {
                                    echo '<span class="check"></span>';
                                } elseif ($td['c'] == "X") {
                                    echo '<span class="cross"></span>';
                                } else {
                                    echo $td['c'];
                                }
                                echo '</td>';
                            }

                            echo '</tr>';
                        }

                        echo '</tbody>';

                        echo '</table>';
                    }
                    ?>
                </div>
            </div>

<?php endif;
    endwhile;

// No value.
else :
// Do something...
endif;

?>