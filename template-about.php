<?php
/*
Template Name: About
*/

get_header(); ?>
<section id="introduction">
    <div class="container">
        <div class="wrapper">
            <h2><?php the_field('title_introduction'); ?></h2>
            <p><?php the_field('introduction_description'); ?></p>
        </div>
    </div>
</section>
<section id="experiences">
    <div class="bg__el bg__el--rt"></div>
    <div class="container">
        <div class="wrapper row">

            <div class="content col-md-6">
                <h2 class="title"><?php the_field('experience_title'); ?></h2>
                <p><?php the_field('experiences_description'); ?></p>
                <?php

                $link = get_field('cta');
                if (!empty($link)) : ?>
                    <a class="button primary" href="<?php echo $link['url']; ?>" target="<?php $link['target']; ?>"><?php echo $link['title']; ?></a>
                <?php endif; ?>
            </div>
            <div class="img-wrapper col-md-6">
                <?php
                $image = get_field('experiences_image');
                if ($image) {
                    echo wp_get_attachment_image($image, "full");
                }
                ?>
            </div>
        </div>
    </div>
</section>
<section id="certifications">
    <div class="container">
        <div class="content__section">
            <ul class="no-list list-certif row between-md">
                <li>
                    <h2 class="title"><?php the_field('certifications_title'); ?></h2>
                    <p><?php the_field('subtitle_certifications'); ?></p>
                </li>
                <?php
                if (have_rows('certifications')) :
                    while (have_rows('certifications')) : the_row(); ?>

                        <li class="item__certif box">
                            <div class="img-wrapper">
                                <?php $image = get_sub_field('logo');
                                if ($image) {
                                    echo wp_get_attachment_image($image, 'full');
                                }
                                ?>
                            </div>
                            <div class="content">
                                <span class="year"><?php the_sub_field('year'); ?></span>
                                <p><?php the_sub_field('description'); ?></p>
                            </div>
                        </li>

                <?php endwhile;
                endif;
                ?>
            </ul>
        </div>
    </div>
</section>
<section id="cta-rdv">
    <div class="bg__el"></div>
    <div class="container">
        <div class="cta__wrapper box">
            <div class="img-wrapper">
                <?php
                $image = get_field('image_cta');
                if ($image) {
                    echo wp_get_attachment_image($image, "full");
                }
                ?>
            </div>
            <div class="content">
                <h2 class="title"><?php the_field('title_cta'); ?></h2>
                <p><?php the_field('description_cta'); ?></p>
                <?php

                $link = get_field('button');
                if (!empty($link)) : ?>
                    <a class="button primary" href="<?php echo $link['url']; ?>" target="<?php $link['target']; ?>"><?php echo $link['title']; ?></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>