<?php
/*
Template Name: Jobs
*/

get_header(); ?>

<section id="hero">
    <div class="hero__wrapper container">
        <div class="row col-lg-offset-1">
            <div class="hero__content col-lg-7">
                <h1 class="title__page"><?php the_field('jobs_hero_title'); ?></h1>
                <?php if (get_field('jobs_hero_subtitle')) : ?>
                    <h3 class="subtitle__page"><?php the_field('jobs_hero_subtitle'); ?></h3>
                <?php endif; ?>
            </div>
            <div class="hero__illu col-lg-5">
                <?php if (get_field('jobs_hero_image')) :
                    $image = get_field('jobs_hero_image'); ?>
                    <?php echo wp_get_attachment_image($image, 'full'); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<section id="jobs">
    <div class="container">
        <div class="jobs__wrapper">
            <?php get_template_part('parts/page', 'jobs'); ?>
        </div>
    </div>
</section>
<section id="form-jobs">
    <div class="container">
        <h2><?php _e('Candidature spontanée', 'wpand-theme'); ?></h2>
        <h3><?php _e('N’hésitez pas à nous envoyer votre candidature à tout moment !', 'wpand-theme'); ?></h3>
        <div class="content__section">
            <?php echo do_shortcode('[contact-form-7 id="513" title="Jobs"]'); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>