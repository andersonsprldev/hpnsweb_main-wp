<?php

/** 
 * For more info: https://developer.wordpress.org/themes/basics/theme-functions/
 *
 */

// Theme support options
require_once(get_template_directory() . '/functions/theme-support.php');

// Theme options
require_once(get_template_directory() . '/functions/theme-options.php');

// WP Head and other cleanup functions
require_once(get_template_directory() . '/functions/cleanup.php');

// Register scripts and stylesheets
require_once(get_template_directory() . '/functions/enqueue-scripts.php');

// Register custom menus and menu walkers
require_once(get_template_directory() . '/functions/menu.php');

// Register sidebars/widget areas
require_once(get_template_directory() . '/functions/sidebar.php');

// Makes WordPress comments suck less
require_once(get_template_directory() . '/functions/comments.php');

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory() . '/functions/page-navi.php');

// Adds support for multiple languages
require_once(get_template_directory() . '/functions/translation/translation.php');

// Adds site styles to the WordPress editor
// require_once(get_template_directory().'/functions/editor-styles.php'); 

// Remove Emoji Support
require_once(get_template_directory() . '/functions/disable-emoji.php');

// Related post function - no need to rely on plugins
// require_once(get_template_directory().'/functions/related-posts.php'); 

// Use this as a template for custom post types
//  require_once(get_template_directory().'/functions/custom-post-type-clients.php');
//  require_once(get_template_directory().'/functions/custom-post-type-partners.php');
require_once(get_template_directory() . '/functions/custom-post-type-solutions.php');
// require_once(get_template_directory() . '/functions/custom-post-type-testimonials.php');
//  require_once(get_template_directory().'/functions/custom-post-type-team.php');
//  require_once(get_template_directory().'/functions/custom-post-type-jobs.php');
//  require_once(get_template_directory().'/functions/custom-post-type-portfolio.php');

// Customize the WordPress login menu
// require_once(get_template_directory().'/functions/login.php'); 

// Customize search
require_once(get_template_directory() . '/functions/acf-search.php');

// Custom cf7 validation
require_once(get_template_directory() . '/functions/cf7-validation.php');

// Color palette wysiwyg
require_once(get_template_directory() . '/functions/color-palette.php');

// API google map
require_once(get_template_directory() . '/functions/acf-google-map.php');

// Duplicate custom post
require_once(get_template_directory() . '/functions/duplicate-post.php');

// Add custom button to TinyEditor 
require_once(get_template_directory() . '/functions/custom-button-editor.php');
add_editor_style('/dist/css/main.css');
function wp4wp_scripts()
{
  // wp_enqueue_style('main_css', get_template_directory_uri() . '/assets/app.bundle.css', array(), '1.0', false);
  // wp_enqueue_script('main_js', get_template_directory_uri() . '/assets/app.bundle.js', array(), '1.0', true);
}
add_action('wp_enqueue_scripts', 'wp4wp_scripts');

add_theme_support('post-thumbnails');

// Remove p tag from contact form 7

add_filter('wpcf7_autop_or_not', '__return_false');

// Custom footer admin 

function wpfme_footer_admin()
{
  echo 'Theme designed and developed by <a href="https://www.anderson.be" target="_blank">Anderson Digital Agency</a> and powered by <a href="https://wordpres.org" target="_blank">Wordpress</a>';
};
add_filter('admin_footer_admin', 'wpfme_footer_admin');


// Move Yoast to bottom
function yoasttobottom()
{
  return 'low';
}
add_filter('wpseo_metabox_prio', 'yoasttobottom');

// Allow editor to edit theme settings 

$editor = get_role('editor');
$editor->add_cap('edit_theme_options');

