<?php
/*
Template Name: Landing page
*/
get_header(); ?>
<section id="section1" class="modules">
    <div class="parallax bg__el"></div>
    <div class="container">
        <h2 class='title'><?php the_field('module_title'); ?></h2>
        <?php
        if (have_rows('modules')) :
            $count = count(get_field('modules'));
        ?>
        <div class="wrapper row between-md">
            <?php while (have_rows('modules')) : the_row(); ?>
            <?php if ($count == 1) : ?>
            <div class="box">
                <span class="day"><?php the_sub_field('day'); ?></span>
                <h3><?php the_sub_field('title'); ?></h3>
                <div class="content">
                    <?php the_sub_field('description'); ?>
                </div>
            </div>
            <?php elseif ($count == 2) : ?>
            <div class="box col-xs-12 col-lg-5">
                <span class="day"><?php the_sub_field('day'); ?></span>
                <h3><?php the_sub_field('title'); ?></h3>
                <div class="content">
                    <?php the_sub_field('description'); ?>
                </div>
            </div>
            <?php else : ?>
            <div class="box col-xs-12 col-lg-4">
                <span class="day"><?php the_sub_field('day'); ?></span>
                <h3><?php the_sub_field('title'); ?></h3>
                <div class="content">
                    <?php the_sub_field('description'); ?>
                </div>
            </div>
            <?php endif; ?>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
</section>

<section id="section2" class="objectifs">
    <div class="bg__el bg__el--rt parallax"></div>
    <div class="container">
        <h2><?php the_field('objectives_title'); ?></h2>
        <?php
        if (have_rows('objectives')) :
            while (have_rows('objectives')) : the_row(); ?>
        <div class="module__wrapper">
            <div class="box content">
                <h3><?php the_sub_field('objective_title'); ?></h3>
                <?php the_sub_field('objective_description'); ?>
            </div>
            <?php $image = get_sub_field('objective_image');
                    if ($image) {
                        echo wp_get_attachment_image($image, 'full');
                    }
                    ?>
        </div>
        <?php endwhile;
        endif; ?>
    </div>
</section>
<section id="section3" class="informations">
    <!-- <div class="parallax bg__el"></div> -->
    <div class="container">
        <div class="wrapper">
            <h2 class="title"><?php the_field('information_title'); ?></h2>
            <div class="prices">
                <div class="box__infos box-price">
                    <?php if (get_field('promotion_formation')) : ?>
                    <span class="initial-price"><?php the_field('price_formation'); ?>€</span>
                    <span class="price"><?php the_field('promotion_price_formation'); ?>€</span>
                    <span class="desc">offre valable jusqu'au <?php the_field('end_date_promotion'); ?></span>
                    <?php else : ?>
                    <span class="price"><?php the_field('price_formation'); ?>€</span>
                    <?php endif; ?>
                </div>
            </div>
            <div class="box row">
                <?php
                if (have_rows('infos')) :
                    while (have_rows('infos')) : the_row(); ?>
                <ul class="list-info row col-xs-12">
                    <li class="col-xs-12 col-md-6"><i class="fas fa-laptop"></i><?php if (get_sub_field('type_formation')) {
                                                                                            echo "Formation en présentiel";
                                                                                        } else {
                                                                                            echo "Formation à distance";
                                                                                        }; ?></li>
                    <li class="col-xs-12 col-md-6"><i class="far fa-clock"></i><?php the_field('duration_formation'); ?>
                        heures de formation</li>
                    <li class="col-xs-12 col-md-6"><i
                            class="fas fa-book-open"></i><?php the_sub_field('number_module'); ?></li>
                    <li class="col-xs-12 col-md-6"><i
                            class="fas fa-graduation-cap"></i><?php the_sub_field('attestation'); ?></li>
                </ul>
                <?php if (get_sub_field('bonus')) : ?>
                <div class="bonus">
                    <span><?php the_sub_field('bonus_description'); ?></span>
                </div>
                <?php endif; ?>
                <?php endwhile;
                endif; ?>
            </div>
            <div id="calendar" class="calendar">
                <div class="dates-wrapper">
                    <?php
                    if (have_rows('list_dates')) :
                        $total = count(get_field('list_dates'));
                    ?>
                    <ul id="list-dates" class="list-dates no-list">
                        <h3>Sessions programmées</h3>
                        <?php
                            while (have_rows('list_dates')) : the_row(); ?>
                        <li class="item-date row between-xs <?php if (!get_sub_field('status')) {
                                                                        echo "complete";
                                                                    }; ?>">
                            <h4 data-name="<?php the_title(); ?>">
                                <span class="date"><?php the_sub_field('dates'); ?></span>
                                <span class="hour"><?php the_sub_field('duration'); ?></span>
                            </h4>

                            <?php if (!get_sub_field('status')) : ?>
                            <a class="button secondary" href="">Complet !</a>
                            <?php else : ?>
                            <a class="button secondary openModal" href="">Réserver</a>
                            <?php endif; ?>

                        </li>
                        <?php endwhile; ?>
                    </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
</section>
<?php if (get_field('button_evaluation')) : ?>

<section id="cta-tool" style="background-image: url('<?php the_field('banner_cta_tool'); ?>');">
    <div class="container">
        <div class="box">
            <h2 class="title"><?php the_field('cta_tool_title'); ?></h2>
            <?php the_field('cta_tool_content'); ?>
            <div class="cta-group">
                <?php
                    $link = get_field('cta_tool_btn');
                    if (!empty($link)) : ?>
                <a class='button secondary' href='<?php echo $link['url']; ?>' target='<?php $link['target']; ?>'><i
                        class="far fa-file-alt"></i><?php echo $link['title']; ?></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<section id="section4" class="about">
    <div class="container">
        <h2 class="title title--ctr"><?php the_field('about_title'); ?></h2>
        <div class="row">
            <div class="content__infos col-md-5">
                <?php $image = get_field('about_image');
                if ($image) {
                    echo wp_get_attachment_image($image, 'full');
                }
                ?>
                <ul>
                    <li><a href="<?php the_field('about_facebook'); ?>"><i class="fab fa-facebook"></i></a></li>
                    <li><a href="<?php the_field('about_linkedin'); ?>"><i class="fab fa-linkedin-in"></i></a></li>
                    <!-- <li><a href="<?php the_field('about_instagram'); ?>"><i class="fab fa-instagram"></i></a></li> -->
                </ul>
                <a href="/a-propos" class="button primary" target="_blank">À propos de Christel</a>
            </div>
            <div class="col-md-6 col-md-offset-1">
                <div class="content">
                    <?php the_field('about_content'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if(get_field('zoom_title')): ?>
<section id="zoom" style="background-image: url('<?php the_field('zoom_image'); ?>');">
    <div class="container">
        <div class="box">
            <h2 class="title title--ctr"><?php the_field('zoom_title'); ?></h2>
            <?php the_field('zoom_content'); ?>
            <div class="group-cta">
                <a class="link" href="<?php the_field('zoom_guide'); ?>" target="_blank"><i
                        class="fas fa-file-download"></i> Guide d'utilisation</a>
                <a class="link" href="<?php the_field('zoom_setup_url'); ?>" target="_blank"><i
                        class="fas fa-file-download"></i> Télécharger Zoom</a>
            </div>
        </div>
    </div>
</section>
<?php endif;?>
<section id="section5" class="steps">
    <div class="bg__el bg__el--rt parallax"></div>
    <div class="container">
        <h2 class="title title--ctr"><?php the_field('steps_title'); ?></h2>
        <div class="box">
            <div class="row between-md">
                <div class="col-sm-6 col-md-4 col-lg-2 center-lg">
                    <img src="" alt="">
                    <span class="number">1.</span>
                    <p><?php the_field('step_1'); ?></p>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-2 center-lg">
                    <img src="" alt="">
                    <span class="number">2.</span>
                    <p><?php the_field('step_2'); ?></p>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-2 center-lg">
                    <img src="" alt="">
                    <span class="number">3.</span>
                    <p><?php the_field('step_3'); ?></p>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-2 center-lg">
                    <img src="" alt="">
                    <span class="number">4.</span>
                    <p><?php the_field('step_4'); ?></p>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-2 center-lg">
                    <img src="" alt="">
                    <span class="number">5.</span>
                    <p><?php the_field('step_5'); ?></p>
                </div>
            </div>

            <div class="group-cta">
                <?php
                $link = get_field('steps_cta');
                if (!empty($link)) : ?>
                <a class='button primary' href='#section3'><?php echo $link['title']; ?></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<section id="section6" class="testimonials">
    <!-- <div class="bg__el parallax"></div> -->
    <div class="container">
        <h2 class="title"><?php the_field('testimonials_title'); ?></h2>
    </div>
    <div class="swiper-container swiper-testimonials-landing">
        <div class="container">
            <div class="swiper-button-prev btn-prev-testimonials-landing"></div>
        </div>
        <div class="swiper-wrapper">
            <?php

            if (have_rows('content')) : ?>

            <?php while (have_rows('content')) : the_row(); ?>

            <div class="swiper-slide box">
                <?php the_sub_field('temoignage'); ?>
            </div>

            <?php endwhile;
            endif;
            ?>
        </div>
        <div class="swiper-button-next btn-next-testimonials-landing"></div>
</section>
<div id="myModal" class="modal">
    <div class="modal-content">
        <span class="close">&times;</span>
        <div class="content">
            <h2><?php _e('Je m\'inscris à la formation', 'wpand-theme'); ?></h2>
            <p><?php _e("Vous désirez vous inscrire à cette formation ? Envoyez-moi votre demande et je vous recontacte très rapidement.", 'wpand-theme'); ?>
            </p>
        </div>
        <div class="popup form">
            <?php echo do_shortcode(get_field('form_shortcode')); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
<script>
    jQuery(document).ready(function ($) {
        var button;
        $('.openModal').click(function () {
            button = $(this);
        });
        var wpcf7ElmSolution = document.querySelector('.modal-content .wpcf7');
        if (wpcf7ElmSolution) {
            wpcf7ElmSolution.addEventListener('wpcf7mailsent', function (event) {
                $('#myModal').hide();
                setTimeout(() => {
                    button.text('Demande envoyée !');
                    $('.button.openModal').addClass('success');
                }, 500);

            }, false);
        }
    });
</script>