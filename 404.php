<?php

/**
 * The template for displaying 404 (page not found) pages.
 *
 * For more info: https://codex.wordpress.org/Creating_an_Error_404_Page
 */

get_header(); ?>

<div class="content">

	<main class="main" role="main">

		<article class="content-not-found">

			<header class="article-header">
				<div class="container">
					<h1><?php _e('404', 'wpand-theme'); ?></h1>
					<h2><?php _e('Cette page n\'existe pas ou a été supprimée', 'wpand-theme'); ?></h2>
					<p>
						<a href="/" class="button primary"> Retour à l'accueil</a>
					</p>
				</div>
			</header> <!-- end article header -->

			<section class="entry-content">
				
			</section> <!-- end article section -->

		</article> <!-- end article -->

	</main> <!-- end #main -->

</div> <!-- end #inner-content -->


<?php get_footer(); ?>