<?php
/*
Template Name: Consultations
*/
get_header(); ?>

<section id="search-bar">
    <div class="container--sm">
        <h2 class="title">Que recherchez-vous ?</h2>
        <ul class="no-list row">
            <?php
            $terms = get_terms('consultations_type');

            foreach ($terms as $term) {
                wp_reset_query();
                $args = array(
                    'post_type' => 'solutions',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'consultations_type',
                            'field' => 'slug',
                            'terms' => array($term->slug)
                        ),
                    ),
                );
                $loop = new WP_Query($args);
                if ($loop->have_posts()) { ?>
                    <li class="item">
                        <a href="#<?php echo $term->slug; ?>">
                            <button class=" button secondary title title-consultations <?php echo $term->slug; ?>">
                                <span class="title-button">Consultations</span>
                                <?php echo $term->name ?>
                            </button>
                        </a>
                    </li>
            <?php }
            }
            ?>
        </ul>
    </div>
</section>
<section id="consultations">
    <div class="container">
        <ul class="list list-categories">
            <?php
            $terms = get_terms('consultations_type');

            foreach ($terms as $term) {
                wp_reset_query();
                $args = array(
                    'post_type' => 'solutions',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'consultations_type',
                            'field' => 'slug',
                            'terms' => array($term->slug),
                        ),
                    ),
                );

                $loop = new WP_Query($args);
                if ($loop->have_posts()) { ?>
                    <li class="item-category box" id="<?php echo $term->slug; ?>">
                        <h2 class="title title-consultations"><?php echo $term->name ?></h2>
                        <ul class="list-consultations no-list">
                            <?php
                            while ($loop->have_posts()) : $loop->the_post(); ?>
                                <li class="item-consultation row between-md">
                                    <div class="img-wrapper col-lg-4">
                                        <?php the_post_thumbnail('full'); ?>
                                    </div>
                                    <div class="content col-lg-7">
                                        <h2 class="title"><?php the_title(); ?></h2>
                                        <p><?php the_field('solution_description'); ?></p>
                                        <div class="cta-group row middle-xs">
                                            <a href="/rendez-vous" class="button rounded"><i class="fas fa-calendar-alt"></i></a>
                                            <a href="tel:<?php the_field('telephone', 'options'); ?>" class="button rounded"><i class="fas fa-phone"></i></a>
                                            <a href="/faq" class="button rounded"><i class="fas fa-question"></i></a>
                                            <!-- <a href="<?php the_permalink(); ?>" class="button primary">Voir la consultation</a> -->

                                        </div>
                                    </div>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    </li>
            <?php }
            }
            ?>
        </ul>
    </div>
</section>

<?php get_footer(); ?>