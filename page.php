<?php 
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 */

get_header(); ?>



<main class="" role="main">

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<?php get_template_part('modules/module', 'flexible-full'); ?>

	<?php endwhile; endif; ?>

</main> <!-- end #main -->




<?php get_footer(); ?>