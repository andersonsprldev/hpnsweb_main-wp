<?php
/* joints partners Type Example
This page walks you through creating 
a partners type and taxonomies. You
can edit this one or copy the following code 
to create another one. 

I put this in a separate file so as to 
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/


// let's create the function for the partner
function partner_cpt() { 
	// creating (registering) the partner 
	register_post_type( 'partner', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Partners', 'wpand-theme'), /* This is the Title of the Group */
			'singular_name' => __('partner', 'wpand-theme'), /* This is the individual type */
			'all_items' => __('All partners', 'wpand-theme'), /* the all items menu item */
			'add_new' => __('Add New', 'wpand-theme'), /* The add new menu item */
			'add_new_item' => __('Add New partner', 'wpand-theme'), /* Add New Display Title */
			'edit' => __( 'Edit', 'wpand-theme' ), /* Edit Dialog */
			'edit_item' => __('Edit Post Types', 'wpand-theme'), /* Edit Display Title */
			'new_item' => __('New Post Type', 'wpand-theme'), /* New Display Title */
			'view_item' => __('View Post Type', 'wpand-theme'), /* View Display Title */
			'search_items' => __('Search Post Type', 'wpand-theme'), /* Search partner Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'wpand-theme'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'wpand-theme'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'partners Directory', 'wpand-theme' ), /* partner Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-welcome-learn-more', /* the icon for the partner type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'partner', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => false, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			'show_in_rest' =>true,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'thumbnail', 'revisions', 'sticky')
	 	) /* end of options */
	); /* end of register post type */
	
} 

	// adding the function to the Wordpress init
	add_action( 'init', 'partner_cpt');
