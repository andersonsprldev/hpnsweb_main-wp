<?php 

// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
    }

    // Register our callback to the appropriate filter
    add_filter('mce_buttons_2', 'my_mce_buttons_2');

    add_action( 'after_setup_theme', 'mythemeslug_theme_setup' );

if ( ! function_exists( 'mythemeslug_theme_setup' ) ) {
	function mythemeslug_theme_setup(){
		/********* Registers an editor stylesheet for the theme ***********/
		add_action( 'admin_init', 'mythemeslug_theme_add_editor_styles' );
		/********* TinyMCE Buttons ***********/
		add_action( 'init', 'mythemeslug_buttons' );
	}
}

/********* Registers an editor stylesheet for the theme ***********/
if ( ! function_exists( 'mythemeslug_theme_add_editor_styles' ) ) {
	function mythemeslug_theme_add_editor_styles() {
	    add_editor_style( 'custom-editor-style.css' );
	}
}

/********* TinyMCE Buttons ***********/
if ( ! function_exists( 'mythemeslug_buttons' ) ) {
	function mythemeslug_buttons() {
		if ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) {
	        return;
	    }

	    if ( get_user_option( 'rich_editing' ) !== 'true' ) {
	        return;
	    }

	    add_filter( 'mce_external_plugins', 'mythemeslug_add_buttons' );
	    add_filter( 'mce_buttons', 'mythemeslug_register_buttons' );
	}
}

if ( ! function_exists( 'mythemeslug_add_buttons' ) ) {
	function mythemeslug_add_buttons( $plugin_array ) {
	    $plugin_array['2-col'] = get_template_directory_uri().'/functions/tinymce/tinymce_buttons-2col.js';
	    $plugin_array['3-col'] = get_template_directory_uri().'/functions/tinymce/tinymce_buttons-3col.js';
		$plugin_array['primary'] = get_template_directory_uri().'/functions/tinymce/tinymce_buttons-primary.js';
		$plugin_array['secondary'] = get_template_directory_uri().'/functions/tinymce/tinymce_buttons-secondary.js';

	    return $plugin_array;
	}
}

if ( ! function_exists( 'mythemeslug_register_buttons' ) ) {
	function mythemeslug_register_buttons( $buttons ) {
	    array_push( $buttons, '2-col' );
	    array_push( $buttons, '3-col' );
		array_push( $buttons, 'primary' );
		array_push( $buttons, 'secondary' );

	    return $buttons;
	}
}

/**
 * Add .entry-content to the body class of TinyMCE.
 * @param array $settings TinyMCE settings.
 * @return array TinyMCE settings.
 */
function wpse_editor_styles_class( $settings ) {
    $settings['body_class'] = 'entry-content';
    return $settings;
}
add_filter( 'tiny_mce_before_init', 'wpse_editor_styles_class' );