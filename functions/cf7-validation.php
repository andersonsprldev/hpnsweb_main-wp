<?php

function alter_wpcf7_posted_data( $data ) {
    if($_POST['radio-choice'] == "by mail") {
        $_POST['your-phone'] = "000 000 00000";
    }
    if($_POST['radio-choice'] == "by phone") {
        $_POST['your-email'] = "contactbyphone@aim-associes.com";
    }
    return $data;
}
add_filter("wpcf7_posted_data", "alter_wpcf7_posted_data");