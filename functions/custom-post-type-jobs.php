<?php
/* joints jobs Type Example
This page walks you through creating 
a jobs type and taxonomies. You
can edit this one or copy the following code 
to create another one. 

I put this in a separate file so as to 
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/


// let's create the function for the jobs
function jobs_cpt() { 
	// creating (registering) the jobs 
	register_post_type( 'jobs', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this jobs
		array('labels' => array(
			'name' => __('Jobs', 'wpand-theme'), /* This is the Title of the Group */
			'singular_name' => __('jobs', 'wpand-theme'), /* This is the individual type */
			'all_items' => __('All jobs', 'wpand-theme'), /* the all items menu item */
			'add_new' => __('Add New', 'wpand-theme'), /* The add new menu item */
			'add_new_item' => __('Add New job', 'wpand-theme'), /* Add New Display Title */
			'edit' => __( 'Edit', 'wpand-theme' ), /* Edit Dialog */
			'edit_item' => __('Edit job', 'wpand-theme'), /* Edit Display Title */
			'new_item' => __('New job', 'wpand-theme'), /* New Display Title */
			'view_item' => __('View job', 'wpand-theme'), /* View Display Title */
			'search_items' => __('Search job', 'wpand-theme'), /* Search jobs Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'wpand-theme'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'wpand-theme'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Jobs custom post type', 'wpand-theme' ), /* jobs Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 9, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-awards', /* the icon for the jobs type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'jobs', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => false, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'thumbnail', 'revisions', 'sticky')
	 	) /* end of options */
	); /* end of register jobs */
	
} 

	// adding the function to the Wordpress init
	add_action( 'init', 'jobs_cpt');

	