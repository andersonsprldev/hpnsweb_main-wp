<?php
/* joints team Type Example
This page walks you through creating 
a team type and taxonomies. You
can edit this one or copy the following code 
to create another one. 

I put this in a separate file so as to 
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/


// let's create the function for the team
function team_cpt() { 
	// creating (registering) the team 
	register_post_type( 'team', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this team member
		array('labels' => array(
			'name' => __('Team members', 'wpand-theme'), /* This is the Title of the Group */
			'singular_name' => __('Team member', 'wpand-theme'), /* This is the individual type */
			'all_items' => __('All Team', 'wpand-theme'), /* the all items menu item */
			'add_new' => __('Add New', 'wpand-theme'), /* The add new menu item */
			'add_new_item' => __('Add New team member', 'wpand-theme'), /* Add New Display Title */
			'edit' => __( 'Edit', 'wpand-theme' ), /* Edit Dialog */
			'edit_item' => __('Edit team member', 'wpand-theme'), /* Edit Display Title */
			'new_item' => __('New team member', 'wpand-theme'), /* New Display Title */
			'view_item' => __('View team member', 'wpand-theme'), /* View Display Title */
			'search_items' => __('Search team member', 'wpand-theme'), /* Search team Title */ 
			'not_found' =>  __('Nothing found in the Database.', 'wpand-theme'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash', 'wpand-theme'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the example team type', 'wpand-theme' ), /* team Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => 'dashicons-businessman', /* the icon for the team type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array( 'slug' => 'team', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'team', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			// 'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 	) /* end of options */
	); /* end of register team member */
} 

	// adding the function to the Wordpress init
	add_action( 'init', 'team_cpt');