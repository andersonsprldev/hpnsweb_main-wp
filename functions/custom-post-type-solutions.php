<?php
/* joints solution Type Example
This page walks you through creating 
a solution type and taxonomies. You
can edit this one or copy the following code 
to create another one. 

I put this in a separate file so as to 
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/


// let's create the function for the solution
function solutions_cpt()
{
	// creating (registering) the solution 
	register_post_type(
		'solutions', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array(
			'labels' => array(
				'name' => __('Solutions', 'wpand-theme'), /* This is the Title of the Group */
				'singular_name' => __('solutions', 'wpand-theme'), /* This is the individual type */
				'all_items' => __('All solutions', 'wpand-theme'), /* the all items menu item */
				'add_new' => __('Add New', 'wpand-theme'), /* The add new menu item */
				'add_new_item' => __('Add New solution', 'wpand-theme'), /* Add New Display Title */
				'edit' => __('Edit', 'wpand-theme'), /* Edit Dialog */
				'edit_item' => __('Edit this solution', 'wpand-theme'), /* Edit Display Title */
				'new_item' => __('New solution', 'wpand-theme'), /* New Display Title */
				'view_item' => __('View this solution', 'wpand-theme'), /* View Display Title */
				'search_items' => __('Search a solution', 'wpand-theme'), /* Search solution Title */
				'not_found' =>  __('Nothing found in the Database.', 'wpand-theme'), /* This displays if there are no entries yet */
				'not_found_in_trash' => __('Nothing found in Trash', 'wpand-theme'), /* This displays if there is nothing in the trash */
				'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __('solutions Directory', 'wpand-theme'), /* solution Description */
			'public' => true,
			'publicly_queryable' => false,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 7, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' => 'dashicons-lightbulb', /* the icon for the solution type menu. uses built-in dashicons (CSS class name) */
			'rewrite'	=> array('slug' => 'solutions', 'with_front' => false), /* you can specify its url slug */
			'has_archive' => false, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			'show_in_rest' => true,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array('title', 'editor', 'thumbnail', 'revisions', 'sticky')
		) /* end of options */
	); /* end of register post type */
	add_post_type_support( 'solutions', 'thumbnail' ); 
}

// adding the function to the Wordpress init
add_action('init', 'solutions_cpt');



// now let's add solution lines (these act like categories)
register_taxonomy(
	'solutions_lines',
	array('solutions'), /* if you change the name of register_post_type( 'solution', then you have to change this */
	array(
		'hierarchical' => true,     /* if this is true, it acts like categories */
		'labels' => array(
			'name' => __('Services', 'wpand-theme'), /* name of the custom taxonomy */
			'singular_name' => __('solutions_line', 'wpand-theme'), /* single taxonomy name */
			'search_items' =>  __('Search solution lines', 'wpand-theme'), /* search title for taxomony */
			'all_items' => __('All solutions lines', 'wpand-theme'), /* all title for taxonomies */
			'parent_item' => __('Parent solutions lines', 'wpand-theme'), /* parent title for taxonomy */
			'parent_item_colon' => __('Parent solutions_lines:', 'wpand-theme'), /* parent taxonomy title */
			'edit_item' => __('Edit solutions line', 'wpand-theme'), /* edit custom taxonomy title */
			'update_item' => __('Update solutions line', 'wpand-theme'), /* update title for taxonomy */
			'add_new_item' => __('Add New solutions line', 'wpand-theme'), /* add new title for taxonomy */
			'new_item_name' => __('New solutions line Name', 'wpand-theme') /* name title for taxonomy */
		),
		'show_admin_column' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => array('slug' => 'solutionlines'),
	)
);

// now let's add Consutations types (these act like categories)
register_taxonomy(
	'consultations_type',
	array('solutions'), /* if you change the name of register_post_type( 'course', then you have to change this */
	array(
		'hierarchical' => true,    /* if this is false, it acts like tags */
		'labels' => array(
			'name' => __('Consultations Type', 'wpand-theme'), /* name of the custom taxonomy */
			'singular_name' => __('consultation_type', 'wpand-theme'), /* single taxonomy name */
			'search_items' =>  __('Search types', 'wpand-theme'), /* search title for taxomony */
			'all_items' => __('All types', 'wpand-theme'), /* all title for taxonomies */
			'parent_item' => __('Parent type', 'wpand-theme'), /* parent title for taxonomy */
			'parent_item_colon' => __('Parent type:', 'wpand-theme'), /* parent taxonomy title */
			'edit_item' => __('Edit type', 'wpand-theme'), /* edit custom taxonomy title */
			'update_item' => __('Update type', 'wpand-theme'), /* update title for taxonomy */
			'add_new_item' => __('Add New type', 'wpand-theme'), /* add new title for taxonomy */
			'new_item_name' => __('New topic type', 'wpand-theme') /* name title for taxonomy */
		),
		'show_admin_column' => true,
		'show_ui' => true,
		'query_var' => true,
		'show_in_rest' => true,
	)
);

// now let's add training types (these act like categories)
register_taxonomy(
	'training_type',
	array('solutions', 'consultations_type'), /* if you change the name of register_post_type( 'course', then you have to change this */
	array(
		'hierarchical' => true,    /* if this is false, it acts like tags */
		'labels' => array(
			'name' => __('Formations Type', 'wpand-theme'), /* name of the custom taxonomy */
			'singular_name' => __('formations_type', 'wpand-theme'), /* single taxonomy name */
			'search_items' =>  __('Search types', 'wpand-theme'), /* search title for taxomony */
			'all_items' => __('All types', 'wpand-theme'), /* all title for taxonomies */
			'parent_item' => __('Parent type', 'wpand-theme'), /* parent title for taxonomy */
			'parent_item_colon' => __('Parent type:', 'wpand-theme'), /* parent taxonomy title */
			'edit_item' => __('Edit type', 'wpand-theme'), /* edit custom taxonomy title */
			'update_item' => __('Update type', 'wpand-theme'), /* update title for taxonomy */
			'add_new_item' => __('Add New type', 'wpand-theme'), /* add new title for taxonomy */
			'new_item_name' => __('New topic type', 'wpand-theme') /* name title for taxonomy */
		),
		'show_admin_column' => true,
		'show_ui' => true,
		'query_var' => true,
		'show_in_rest' => true,
	)
);

// now let's add training categories (webinars, coaching,...) (these act like categories)
register_taxonomy(
	'training_cat',
	array('solutions'), /* if you change the name of register_post_type( 'course', then you have to change this */
	array(
		'hierarchical' => true,    /* if this is false, it acts like tags */
		'labels' => array(
			'name' => __('Training Categories', 'wpand-theme'), /* name of the custom taxonomy */
			'singular_name' => __('training_cat', 'wpand-theme'), /* single taxonomy name */
			'search_items' =>  __('Search category', 'wpand-theme'), /* search title for taxomony */
			'all_items' => __('All categories', 'wpand-theme'), /* all title for taxonomies */
			'parent_item' => __('Parent category', 'wpand-theme'), /* parent title for taxonomy */
			'parent_item_colon' => __('Parent category:', 'wpand-theme'), /* parent taxonomy title */
			'edit_item' => __('Edit category', 'wpand-theme'), /* edit custom taxonomy title */
			'update_item' => __('Update category', 'wpand-theme'), /* update title for taxonomy */
			'add_new_item' => __('Add New category', 'wpand-theme'), /* add new title for taxonomy */
			'new_item_name' => __('New category', 'wpand-theme') /* name title for taxonomy */
		),
		'show_admin_column' => true,
		'show_ui' => true,
		'query_var' => true,
		'show_in_rest' => true,
	)
);