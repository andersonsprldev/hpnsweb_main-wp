<?php
function site_scripts()
{
  global $wp_styles; // Call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

  // Adding scripts file in the footer
  wp_enqueue_script('site-js', get_template_directory_uri() . '/dist/js/app.js', array('jquery'), filemtime(get_template_directory() . '/src/js'), true);

  // CDN Swiper (Sliders)
  wp_register_script('Swiper', 'https://cdnjs.cloudflare.com/ajax/libs/Swiper/5.3.8/js/swiper.min.js', null, null, true);
  wp_enqueue_script('Swiper');

  // CDN jQuery Validation (form validation jQuery)
  wp_register_script('jquery-validate', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js', null, null, true);
  wp_enqueue_script('jquery-validate');

    // CDN Lightbox
    wp_register_script('Lightbox', 'https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox.min.js', null, null, true);
    wp_enqueue_script('Lightbox');
  
  // CDN AOS (animations)
  wp_register_script('AOS', 'https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js', null, null, true);
  wp_enqueue_script('AOS');

    // CDN parallax 
    wp_register_script('parallax', 'https://cdn.jsdelivr.net/parallax.js/1.4.2/parallax.min.js', null, null, true);
    wp_enqueue_script('parallax');

      // CDN Masonry

  wp_register_script('Masonry', 'https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js', null, null, true);
  wp_enqueue_script('Masonry');
  
  // CDN addThis (Social share buttons)
  // wp_register_script('addThis', '//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5c08ebe2ed8a1472', null, null, true);
  // wp_enqueue_script('addThis');

  // CDN imagesLoaded (load images before isotope)
  wp_register_script('imagesLoaded', 'https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js', null, null, false);
  wp_enqueue_script('imagesLoaded');


    // Google Map API
    // wp_register_script('googleMap', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDq6lxBeHVayKD5Th_RGIPQCvnTgmZ2qmU', null, null, true);
    // wp_enqueue_script('googleMap');

  // Register main stylesheet
  wp_enqueue_style('site-css', get_template_directory_uri() . '/dist/css/main.css', array(), filemtime(get_template_directory() . '/dist'), 'all');
  wp_enqueue_style( 'swiper', "https://cdnjs.cloudflare.com/ajax/libs/Swiper/5.4.5/css/swiper.min.css" );
  wp_enqueue_style( 'aos', "https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css" );
  wp_enqueue_style('lightbox', 'https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.css');

  // Comment reply script for threaded comments
  if (is_singular() and comments_open() and (get_option('thread_comments') == 1)) {
    wp_enqueue_script('comment-reply');
  }
}
add_action('wp_enqueue_scripts', 'site_scripts', 999);