(function() {
    tinymce.PluginManager.add( '2-col', function( editor, url ) {
        // Add Button to Visual Editor Toolbar
        editor.addButton('2-col', {
            title: 'Insérer 2 colonnes',
            cmd: '2-col',
            image: url + '/2-col.svg',
        });

        editor.addCommand('2-col', function() {
            var selected_text = editor.selection.getContent({
                'format': 'html'
            });
            var col2 = '<div class="row between-md"><div class="col2 col-xs-12 col-md-5 col-md-offset-1"><p></p></div><div class="col2 col-xs-12 col-md-5 col-md-offset-1"><p></p></div></div>';
            var return_text = '';
            return_text = col2;
            editor.execCommand('mceReplaceContent', false, return_text);
            return;
        });
    });
})();