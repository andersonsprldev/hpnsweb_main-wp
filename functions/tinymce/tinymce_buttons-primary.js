(function () {
    tinymce.PluginManager.add('primary', function (editor, url) {
        // Add Button to Visual Editor Toolbar
        editor.addButton('primary', {
            title: 'Insérer un bouton primaire',
            cmd: 'primary',
            image: url + '/primary-btn.svg',

            onClick: function () {
                editor.windowManager.open({
                    title: 'Insérer un bouton primaire',
                    body: [{
                            type: 'textbox',
                            name: 'title',
                            placeholder: 'Texte du bouton',
                            multiline: true,
                            minWidth: 350,
                            minHeight: 50,
                        },
                        {
                            type: 'textbox',
                            name: 'link',
                            placeholder: 'Lien du bouton',
                            multiline: false,

                        }
                    ],

                    onSubmit: function (e) {
                        editor.insertContent('<a class="button primary" href="' + e.data.link + '">' + e.data.title + '</a>');
                    }
                });
            }
        });

    });
})();