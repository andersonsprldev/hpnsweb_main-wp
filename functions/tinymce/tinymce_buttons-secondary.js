(function () {
    tinymce.PluginManager.add('secondary', function (editor, url) {
        // Add Button to Visual Editor Toolbar
        editor.addButton('secondary', {
            title: 'Insérer un bouton secondaire',
            cmd: 'secondary',
            image: url + '/secondary-btn.svg',

            onClick: function () {
                editor.windowManager.open({
                    title: 'Insérer un bouton secondaire',
                    body: [{
                            type: 'textbox',
                            name: 'title',
                            placeholder: 'Texte du bouton',
                            multiline: true,
                            minWidth: 350,
                            minHeight: 50,
                        },
                        {
                            type: 'textbox',
                            name: 'link',
                            placeholder: 'Lien du bouton',
                            multiline: false,

                        }
                    ],

                    onSubmit: function (e) {
                        editor.insertContent('<a class="button secondary" href="' + e.data.link + '">' + e.data.title + '</a>');
                    }
                });
            }
        });

    });
})();