(function() {
    tinymce.PluginManager.add( '3-col', function( editor, url ) {
        // Add Button to Visual Editor Toolbar
        editor.addButton('3-col', {
            title: 'Insérer 3 colonnes',
            cmd: '3-col',
            image: url + '/3-col.svg',
        });

        editor.addCommand('3-col', function() {
            var selected_text = editor.selection.getContent({
                'format': 'html'
            });
            var col3 = '<div class="row between-md"><div class="col3 col-xs-12 col-md-3 col-md-offset-1"><p></p></div><div class="col3 col-xs-12 col-md-3 col-md-offset-1"><p></p></div><div class="col3 col-xs-12 col-md-3 col-md-offset-1"><p></p></div></div>';
            var return_text = '';
            return_text = col3;
            editor.execCommand('mceReplaceContent', false, return_text);
            return;
        });
    });
})();