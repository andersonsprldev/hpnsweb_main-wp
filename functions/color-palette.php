<?php



/*
* Customize the default color palette for TinyMce editor
*/
function wpr_custom_color_tinymce($options)
{
    $options['textcolor_map'] = json_encode(
        array(
            "1F4653", "Bleu-pétrole100",
            "586C85", "Bleu-gris100",
            "2FABB9", "Bleu-turquoise100",
            "6795A3", "Gris100",
            "8AA27B", "Vert100",
            "E14C2A", "Rouge100",
            "DDC777", "Jaune100",
            "52A4BE", "Bleu-pétrole50",
            "D8DEE5", "Bleu-gris50",
            "A5E2E9", "Bleu-turquoire50",
            "C2D5DA", "Gris50",
            "B8C7AF", "Vert50",
            "ED927E", "Rouge50",
            "EBDDAD", "Jaune50",
            "762210", "Dark-Rouge50",
            "44543A", "Dark-Green50",
            "17555B", "Dark-Bleu-turquoise50",
            "000000", "Black",
            "ffffff", "White"
        )
    );
    return $options;
}
add_filter('tiny_mce_before_init', 'wpr_custom_color_tinymce');

