// webpack.mix.js

let mix = require('laravel-mix');

mix.setPublicPath('dist');

mix.js('src/js/app.js', 'js')
   .copyDirectory('src/images', 'dist/images')
   .sass('src/scss/main.scss', 'css')
   .sourceMaps()
   .options({
      processCssUrls: false
   });