<?php
/*
Template Name: Homepage
*/

get_header(); ?>
<section id="about">
    <div class="parallax bg__el"></div>
    <div class="container">
        <div class="row between-sm middle-md">
            <div class="col-xs-12 col-md-6">
                <div class="img-wrapper">
                    <?php $image = get_field('homepage_about_image');
                    $size = 'full';
                    if ($image) {
                        echo wp_get_attachment_image($image, 'full');
                    }
                    ?>
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <h2 class="title"><?php the_field('homepage_about_title'); ?></h2>
                <div class="content">
                    <p><?php the_field('homepage_about_desc'); ?></p>
                </div>
                <?php
                $link = get_field('homepage_about_cta');
                if ($link) :
                ?>
                    <a class="button primary" href="<?php echo esc_url($link['url']); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link['title']); ?></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<section id="services">
    <div class="container">
        <div id="consultations">
            <div class="bg__el bg__el--rt parallax"></div>
            <h2 class="title"><?php the_field('title_consultations'); ?></h2>
            <ul class="list-item no-list row betwen-md">
                <?php if (have_rows('type_consultations')) :
                    while (have_rows('type_consultations')) : the_row();
                ?>
                        <li class="item box">
                            <div class="img-wrapper">
                                <?php
                                $image = get_sub_field('image');
                                if ($image) {
                                    echo wp_get_attachment_image($image, "full");
                                }
                                ?>
                            </div>
                            <div class="content">
                                <h2 class="title title--sm"><?php the_sub_field('title'); ?></h2>
                                <p><?php the_sub_field('description'); ?></p>
                                <?php
                                $link = get_sub_field('link');
                                if ($link) : ?>
                                    <a class="button link" href="<?php echo esc_url($link); ?>">Vers les consultations</a>
                                <?php endif; ?>
                            </div>
                        </li>
                <?php
                    endwhile;
                endif; ?>
            </ul>
        </div>
        <div id="formations">
            <div class="bg__el parallax"></div>
            <div class="formations__wrapper box">
                <div class="img-wrapper">
                    <?php
                    $image = get_field('image_formations');
                    if ($image) {
                        echo wp_get_attachment_image($image, "full");
                    }
                    ?>
                </div>
                <div class="content">
                    <h2 class="title"><?php the_field('title_formations'); ?></h2>
                    <p><?php the_field('description_formations'); ?></p>
                    <a href="<?php the_field('link_formations'); ?>" class="button link">Voir les formations</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="rdv">
    <div class="container">
        <div class="row between-md">
            <div class="col-md-7">
                <div class="col-md-10">
                    <h2 class="title"><?php the_field('homepage_rdv_title'); ?></h2>
                    <p><?php the_field('homepage_rdv_desc'); ?></p>
                </div>
            </div>
            <div class="col-md-5">
                <div class="img-wrapper right-md">
                    <?php
                    $image = get_field('homepage_rdv_img');
                    if ($image) {
                        echo wp_get_attachment_image($image, "full");
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="testimonials">
    <div class="bg__el parallax"></div>
    <div class="container">
        <div class="row">
            <h2 class="title"><?php the_field('homepage_trust_title'); ?></h2>
            <?php echo do_shortcode('[wprevpro_usetemplate tid="1"]'); ?>
        </div>
    </div>
</section>
<?php get_footer(); ?>