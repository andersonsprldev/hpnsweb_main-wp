<?php

/**
 * The template for displaying the footer. 
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?>
</main>
<footer class="main-footer" role="contentinfo">
	<?php if (!is_page_template('template-landing.php') && !is_page_template('template-form-submitted.php') && !is_page_template('template-lead-magnet.php')) : ?>
		<div class="container footer__wrapper">
			<div class="row">
				<div class="col-xs-12 col-md-4">
					<div class="footer__adress">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="Happynaiss Sage-Femme Sombreffe">
						<ul class="no-list infos">
							<li class="address">
								<a href="https://www.google.be/maps/place/Rue+Ottiamont+28,+5140+Sombreffe/@50.5343235,4.6002381,17z/data=!3m1!4b1!4m5!3m4!1s0x47c180e47d85810f:0xc0db9ea4bb679dde!8m2!3d50.5343235!4d4.6024268">
									<p>Rue Ottiamont, 28, <br>5140 Sombreffe</p>
								</a>
							</li>
							<li class="tel"><a href="tel:0495467865">Par SMS 0495/46.78.65</a></li>
						</ul>
					</div>
				</div>
				<div class="col-xs-12 col-md-4">
					<h3>Navigation</h3>
					<?php if (is_active_sidebar('footer-2')) : ?>
						<div class="footer__nav col-sm-12 col-md-8 col-lg-6">
							<?php dynamic_sidebar('footer-2'); ?>
						</div>
					<?php endif; ?>
				</div>
				<div class="col-xs-12 col-md-4">
					<h3>Suivez Happynaiss</h3>
					<ul class="no-list socials">
						<li>
							<a href="https://www.facebook.com/sagefemmehappynaiss/"><i class="fab fa-facebook"></i> Facebook</a>
						</li>
						<li>
							<a href="https://www.linkedin.com/in/christel-jouret-75aa537a/"><i class="fab fa-linkedin-in"></i> Linkedin</a>
						</li>
						<li>
							<a href="#"><i class="fab fa-instagram"></i> Instagram</a>
						</li>
					</ul>
					<?php echo do_shortcode('[sibwp_form id=1]'); ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div id="credits">
		<div class="container">
			<div class="credits__wrapper container">
				<div class="copyright">
					<p>© <?php echo date('Y'); ?> Happynaiss - <a href="/privacy-policy">Politique de confidentialité</a></p>
				</div>
				<div class="made-by">
					<p>Website by </p>
					<a href="https://www.anderson.be" target="_blank">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 250.43 293.37">
							<defs>
								<style>
									.a {
										fill: #99a5bb;
									}
								</style>
							</defs>
							<path class="a" d="M126.71,255.47C78.64,255.47,38,219.66,38,177.26V165.62c0-32.38,25.6-49.86,49.69-49.86,26.51,0,47.27,18.1,47.27,41.22v5.14l.65.57a11.78,11.78,0,1,1-16.63,1q.3-.34.63-.66l.58-.56V157c0-15.09-14-26.46-32.52-26.46-16.94,0-34.93,12.3-34.93,35.1v11.64c0,34.4,33.85,63.46,73.92,63.46,39.14,0,72.21-29.06,72.21-63.46V140.35l-.67-.57a11.72,11.72,0,0,1-1.43-16.51,10.72,10.72,0,0,1,1-1.09l.6-.56v-3.54c0-35.15-33.78-64.84-73.76-64.84-8,0-19.7,1.71-21.28,1.95h-2.54c-3.12,0-9.17.07-14.68.59A157.79,157.79,0,0,0,65.3,58.94,97,97,0,0,1,124.65,38.5c48.81,0,88.52,35.71,88.52,79.6v3.49l.61.57a11.69,11.69,0,0,1,.85,16.52,5.11,5.11,0,0,1-.45.48l-.55.56v37.59C213.67,220.39,174.66,255.47,126.71,255.47Zm1.16-91.68a7.72,7.72,0,1,0,7.72,7.72h0a7.72,7.72,0,0,0-7.72-7.72Zm78-40.72a7.72,7.72,0,1,0,7.71,7.73v0A7.72,7.72,0,0,0,205.87,123.07Z" />
							<path class="a" d="M124.73,293.37C56,293.37,0,242.88,0,180.83V112.55C0,50.49,56,0,124.73,0c69.21,0,125.6,50.41,125.7,112.38v1.9h0v66.55C250.44,242.88,194.05,293.37,124.73,293.37ZM91,89.61c-44.51,0-79.37,29.74-79.37,67.71v23.51c0,55.62,50.72,100.88,113.06,100.88,62.88,0,114-45.26,114-100.88V110.64h0c-1.15-54.75-51.86-99-114-99-61.87,0-112.58,44.91-113,100.12v5.73L15.11,113C31.78,91,60.17,77.94,91,77.94h1.37c46,0,80.77,32.83,80.77,76.37v16c0,29.84-24.92,45.94-48.37,45.94-25.41,0-44.57-17.27-44.57-40.18v-5.54l-.67-.57a11.65,11.65,0,1,1,16.41-1.42,11.52,11.52,0,0,1-3.12,2.61l-1,.55v4.37c0,16.26,14.14,28.51,32.9,28.51,18.24,0,36.7-11.77,36.7-34.27v-16c0-37.49-29.06-64.7-69.1-64.7Zm-4,63.63a7.86,7.86,0,1,0,7.85,7.87v0a7.86,7.86,0,0,0-7.79-7.85Z" />
						</svg>
					</a>
				</div>
			</div>
		</div>
	</div>
</footer> <!-- end .footer -->
<?php if (!is_page_template('template-landing.php') || is_page_template('template-form-submitted.php') || is_page_template('template-lead-magnet.php')) : ?>
	
	<div id="bottom-bar">
		<div class="container">
			<div class="wrapper row">
				<span class="avatar">
					<?php $image = get_field('bar_avatar', 'options');
					if ($image) {
						echo wp_get_attachment_image($image, 'full');
					}
					?>
				</span>
				<p>
					<?php the_field('bar_text', 'options'); ?>
				</p>
				<div class="cta-group">
					<?php
					$link = get_field('bar_cta_1', 'options');
					if ($link) :
					?>
						<a class="button primary" href="<?php echo $link['url']; ?>" target="<?php $link['target']; ?>"><?php echo $link['title']; ?></a>
					<?php endif; ?>
					<?php
					$link2 = get_field('bar_cta_2', 'options');
					if ($link2) :
					?>
						<a class="button secondary" href="<?php echo $link2['url']; ?>" target="<?php $link2['target']; ?>"><?php echo $link2['title']; ?></a>
					<?php endif; ?>
					<span class="close">
						<span class="line line1"></span>
						<span class="line line2"></span>
					</span>
				</div>
			</div>
		</div>
	</div>

<?php endif; ?>

<?php wp_footer(); ?>

<!-- Hide wp admin bar -->

<div id="btn-hide-wpbar">
	<div class="hide-btn">
		off
	</div>
	<div class="show-btn">
		on
	</div>
</div>
</div>
</body>

</html> <!-- end page -->