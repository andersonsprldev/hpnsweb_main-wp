<?php
/*
Template Name: Rendez-vous
*/

get_header(); ?>
<div class="content">
    <div class="container">
        <?php if(get_field('rdv_content')): ?>
        <?php the_field('rdv_content'); ?>
        <?php endif; ?>
    </div>
</div>

<?php get_footer(); ?>