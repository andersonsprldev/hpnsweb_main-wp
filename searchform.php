<?php
/**
 * The template for displaying search form
 */
 ?>

<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
<div class="search__wrapper">
	<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search...', 'wpand-theme' ) ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php echo esc_attr_x( 'Search for:', 'wpand-theme' ) ?>" />
	<label class="submit-search">
		<input type="submit" class="search-submit button" value="<?php echo esc_attr_x( 'Search', 'wpand-theme' ) ?>" />
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 308.2 308.6"><defs><style>.cls-1{fill:#fff;}</style></defs><path class="cls-1" d="M17.46,308.6,0,291.14l105.62-105.6C83.93,157.26,75.34,125.82,82.56,91c6.13-29.62,22-53.41,46.92-70.63A113.17,113.17,0,0,1,276.8,35.82c40.94,43.37,41.2,107.74,4.58,151.46-37.69,45-106.43,56.45-158.23,15.65ZM194.19,24.07a90,90,0,0,0-90.11,90.12c.1,49.54,40.22,89.74,89.61,89.79a90,90,0,1,0,.5-179.91Z"/></svg>
	</label>
</div>
</form>