<?php
/*
Template Name: Lead Magnet
*/
get_header(); ?>
<section id="content">
    <div class="parallax bg__el"></div>
    <div class="container">
        <h2 class="title title--ctr"><?php the_field('content_lead_magnet_title'); ?></h2>
        <div class="wrapper row between-md">
            <div class="box">
                <?php the_field('content_lead_magnet'); ?>
            </div>
        </div>
    </div>
</section>

<section id="cta">
    <div class="container">
        <h2><?php the_field('cta_title'); ?></h2>
        <a id="button-ga-2" class="button secondary openModal button-ga" href=""><i class="far fa-file-alt"></i> Télécharger l'outil</a>
    </div>
</section>

<div id="myModal" class="modal">
    <div class="modal-content">
        <span class="close">&times;</span>
        <div class="content">
            <h2><?php _e('Je télécharge l\'outil d\'évaluation de l\'allaitement', 'wpand-theme'); ?></h2>
            <p><?php _e("Remplissez ce formulaire pour recevoir votre document.", 'wpand-theme'); ?></p>
        </div>
        <div class="popup form">
            <?php echo do_shortcode(get_field('shortcode')); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>
<script>
    jQuery(document).ready(function($) {
        var button;
        $('.openModal').click(function() {
            button = $(this);
        });
        var wpcf7ElmSolution = document.querySelector('.modal-content .wpcf7');
        if (wpcf7ElmSolution) {
            wpcf7ElmSolution.addEventListener('wpcf7mailsent', function(event) {
                $('#myModal').hide();
                setTimeout(() => {
                    var textButtonSuccess = "<span class='success-ga'>Check-allaitement téléchargé !</span>";
                    button.empty();
                    button.append(textButtonSuccess);
                    $('.button.openModal').addClass('success');
                }, 500);

            }, false);
        }
    });
</script>