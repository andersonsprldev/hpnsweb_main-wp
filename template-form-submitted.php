<?php
/*
Template Name: Submitted Form
*/
get_header(); ?>

<section id="section1" class="modules">
    <div class="parallax bg__el"></div>
    <div class="container">
        <h1 class='title'>Merci pour votre demande d'inscription</h1>
            <p>J'ai bien reçu votre demande pour l'inscription à la formation 
                <span id="title-ga" class="title"><?php echo do_shortcode('[get_param param="formation-title"]'); ?></span>
                Vous recevrez bientot de mes nouvelles pour finaliser votre inscription. En attendant, n'hésitez pas à découvrir le site de Happynaiss. 
            </p>
            <div class="group-cta">
                <a href="javascript:history.back()" class="button primary">Retourner sur la page</a>
                <a href="/" class="button secondary">Découvrir happynaiss.net</a>
            </div>
    </div>
</section>

<?php get_footer(); ?>