<?php

/**
 * The template for displaying all single posts and attachments
 */

get_header(); ?>

<div class="content">

	<main role="main">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
					<?php get_template_part('modules/module', 'flexible'); ?>
				</article> <!-- end article -->


			<?php endwhile; ?>


		<?php endif; ?>

	</main> <!-- end #main -->

</div> <!-- end #content -->

<?php get_footer(); ?>