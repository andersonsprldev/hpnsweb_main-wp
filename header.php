<?php

/**
 * The template for displaying the header
 *
 * This is the template that displays all of the <head> section
 *
 */
?>

<!doctype html>

<html class="no-js" <?php language_attributes(); ?>>

<head>
	<?php
	if (get_field('google_tag_manager_script', 'options')) :
		the_field('google_tag_manager_script', 'options');
	endif;
	?>
	<!-- Force IE to use the latest rendering engine available -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- Mobile Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- If Site Icon isn't set in customizer -->
	<?php if (!function_exists('has_site_icon') || !has_site_icon()) { ?>
	<!-- Icons & Favicons -->
	<link rel="apple-touch-icon" sizes="180x180"
		href="<?php echo get_template_directory_uri(); ?>/favicons/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32"
		href="<?php echo get_template_directory_uri(); ?>/favicons/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16"
		href="<?php echo get_template_directory_uri(); ?>/favicons/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/favicons/site.webmanifest">
	<link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/favicons/safari-pinned-tab.svg"
		color="#5552a3">
	<meta name="msapplication-TileColor" content="#5552a3">
	<meta name="theme-color" content="#ffffff">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/all.min.css" rel="stylesheet" />
	<?php } ?>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
	<?php
	if (get_field('google_tag_manager_no_script', 'options')) :
		the_field('google_tag_manager_no_script', 'options');
	endif;
	?>
	<div class="wrapper-view">
		<header class="header main-header" role="banner">
			<div class="header__wrapper">
				<a href="<?php echo home_url(); ?>" class="logo logo-header">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 370.69 288.65">
						<defs>
							<style>
								.cls-1 {
									fill: #faa0d0;
								}

								.cls-2 {
									fill: #912e60;
								}

								.cls-3 {
									fill: none;
									stroke: #912e60;
									stroke-miterlimit: 10;
									stroke-width: 1.4px;
								}
							</style>
						</defs>
						<g id="Calque_2" data-name="Calque 2">
							<g id="Calque_1-2" data-name="Calque 1">
								<path class="cls-1"
									d="M8.56,152.25V105.69l38.68,40.48v-40h3v47.25L11.6,112.94v39.31Z" />
								<path class="cls-1"
									d="M82.68,124.06h2.77v28.19H82.68V147.2a12.86,12.86,0,0,1-11,5.81C64.31,153,58,147.41,58,138.16c0-8.71,5.53-14.72,13.75-14.72a12.86,12.86,0,0,1,11,5.53ZM60.86,138c0,6.35,3.93,12.43,11,12.43S82.82,144.44,82.82,138c0-8.36-6.15-11.88-11-11.88C66.24,126.07,60.86,130.28,60.86,138Z" />
								<path class="cls-1"
									d="M92.56,114.67a2.49,2.49,0,1,1,2.49,2.56A2.46,2.46,0,0,1,92.56,114.67Zm3.87,9.39v28.19H93.67V124.06Z" />
								<path class="cls-1"
									d="M116.67,129.24a5.15,5.15,0,0,0-4.9-3.17,4.54,4.54,0,0,0-4.77,4.49c0,2.76,2.21,3.66,5,4.76s5.46,1.87,7.26,4.15a6.8,6.8,0,0,1,1.45,4.56c0,6.42-4.84,9-9.19,9a9.66,9.66,0,0,1-9.74-7.46l2.69-.83a7.19,7.19,0,0,0,7,5.66,6,6,0,0,0,6.29-6.21c0-3.18-2-4.36-6.29-6.08-2.83-1.18-4.9-2.08-6.15-3.73a5.94,5.94,0,0,1-1.24-3.66c0-4.29,3.25-7.26,7.53-7.26A7.59,7.59,0,0,1,119,128Z" />
								<path class="cls-1"
									d="M138,129.24a5.15,5.15,0,0,0-4.91-3.17,4.54,4.54,0,0,0-4.76,4.49c0,2.76,2.21,3.66,5,4.76s5.46,1.87,7.25,4.15A6.75,6.75,0,0,1,142,144c0,6.42-4.83,9-9.18,9a9.65,9.65,0,0,1-9.74-7.46l2.69-.83a7.19,7.19,0,0,0,7,5.66,6,6,0,0,0,6.28-6.21c0-3.18-2-4.36-6.28-6.08-2.84-1.18-4.91-2.08-6.15-3.73a5.94,5.94,0,0,1-1.24-3.66c0-4.29,3.24-7.26,7.53-7.26a7.6,7.6,0,0,1,7.39,4.56Z" />
								<g id="Layer_2" data-name="Layer 2">
									<g id="Layer_1-2" data-name="Layer 1-2">
										<polygon class="cls-2"
											points="57.92 0 57.92 49.23 5.79 49.23 5.79 0 0 0 0 104.26 5.79 104.26 5.79 55.02 57.92 55.02 57.92 104.26 63.71 104.26 63.71 0 57.92 0" />
										<path class="cls-2"
											d="M277.06,34.75A35.68,35.68,0,0,0,247.13,51V40.54h-5.79v86.89h5.79V89.91a35.7,35.7,0,1,0,29.93-55.16Zm0,65.65A29.93,29.93,0,1,1,307,70.47,29.93,29.93,0,0,1,277.06,100.4Z" />
										<path class="cls-2"
											d="M194,34.75A35.68,35.68,0,0,0,164.11,51V40.54h-5.79v86.89h5.79V89.91A35.7,35.7,0,1,0,194,34.75Zm0,65.65A29.93,29.93,0,1,1,224,70.47,29.93,29.93,0,0,1,194,100.4Z" />
										<path class="cls-2"
											d="M73.37,70.47A35.69,35.69,0,0,0,139,89.91v16.28h5.79V40.54H139V51A35.7,35.7,0,0,0,73.37,70.47Zm5.79,0a29.93,29.93,0,1,1,29.92,29.93A29.93,29.93,0,0,1,79.16,70.47Z" />
									</g>
								</g>
								<path class="cls-2"
									d="M364.9,40.54l-20.33,42.4-20.21-42.4h-5.79l22.78,49.12-18.21,38c.27-1.34-74.32,154.11-168.27,153.3-82.18-.71-105.74-76.69-106.19-88.81-.38-10.3-20.58-15.48-5.79,25.1,14.41,30.8,57.64,73.64,112,71.24C217.53,293.22,292,205,328.35,130.17l42.34-89.63Z" />
								<path class="cls-3"
									d="M153.9,214.33s-7.72-8.94-10.61-8.94-4.8,2.74-8.68,2.74-9.76-7.27-21.3-7.27-34.7,3.82-34.7,34.71,35.84,38.41,40.12,39.48,19.73-4.73,31.31-4.73,17.38,5.8,29,7.73,71.44-29,81.09-54.06-13.51-34.75-21.24-36.69-16.86-1.11-21.94,0-22.21,5.23-18.6,3.87c7.07-2.68,2.61-17.58-11.58-19.31-3.84-.47-7.73,0-7.73,7.72" />
								<path class="cls-3"
									d="M155.83,187.3c-1.93,11.59,19.31,38.62,19.31,38.62l15.45-7.72h30.89" />
								<path class="cls-3" d="M180.93,193.1s-8.47,4.92-8.47,12.11,10.4,13,18.13,13" />
								<path class="cls-3" d="M175.14,225.92s-1.93,30.89,1.93,34.75" />
								<line class="cls-3" x1="126.87" y1="217.23" x2="126.87" y2="209.51" />
							</g>
						</g>
					</svg>
				</a>
				<?php if (!is_page_template('template-landing.php') && !is_page_template('template-form-submitted.php') && !is_page_template('template-lead-magnet.php')) : ?>
				<a href="#" class="burger__menu burger">
					<span></span>
				</a>
				<?php endif; ?>
				<nav id="secondary__nav">
					<div class="container">
						<?php if (!is_page_template('template-landing.php') && !is_page_template('template-form-submitted.php') && !is_page_template('template-lead-magnet.php')) : ?>
						<?php joints_top_secondary_nav(); ?>
						<?php endif; ?>
					</div>
				</nav>
				<nav id="main__nav">
					<div class="container">
						<div class="row middle-lg between-lg">
							<a href="<?php echo home_url(); ?>" class="logo">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 370.69 288.65">
									<defs>
										<style>
											.cls-1 {
												fill: #faa0d0;
											}

											.cls-2 {
												fill: #912e60;
											}

											.cls-3 {
												fill: none;
												stroke: #912e60;
												stroke-miterlimit: 10;
												stroke-width: 1.4px;
											}
										</style>
									</defs>
									<g id="Calque_2" data-name="Calque 2">
										<g id="Calque_1-2" data-name="Calque 1">
											<path class="cls-1"
												d="M8.56,152.25V105.69l38.68,40.48v-40h3v47.25L11.6,112.94v39.31Z" />
											<path class="cls-1"
												d="M82.68,124.06h2.77v28.19H82.68V147.2a12.86,12.86,0,0,1-11,5.81C64.31,153,58,147.41,58,138.16c0-8.71,5.53-14.72,13.75-14.72a12.86,12.86,0,0,1,11,5.53ZM60.86,138c0,6.35,3.93,12.43,11,12.43S82.82,144.44,82.82,138c0-8.36-6.15-11.88-11-11.88C66.24,126.07,60.86,130.28,60.86,138Z" />
											<path class="cls-1"
												d="M92.56,114.67a2.49,2.49,0,1,1,2.49,2.56A2.46,2.46,0,0,1,92.56,114.67Zm3.87,9.39v28.19H93.67V124.06Z" />
											<path class="cls-1"
												d="M116.67,129.24a5.15,5.15,0,0,0-4.9-3.17,4.54,4.54,0,0,0-4.77,4.49c0,2.76,2.21,3.66,5,4.76s5.46,1.87,7.26,4.15a6.8,6.8,0,0,1,1.45,4.56c0,6.42-4.84,9-9.19,9a9.66,9.66,0,0,1-9.74-7.46l2.69-.83a7.19,7.19,0,0,0,7,5.66,6,6,0,0,0,6.29-6.21c0-3.18-2-4.36-6.29-6.08-2.83-1.18-4.9-2.08-6.15-3.73a5.94,5.94,0,0,1-1.24-3.66c0-4.29,3.25-7.26,7.53-7.26A7.59,7.59,0,0,1,119,128Z" />
											<path class="cls-1"
												d="M138,129.24a5.15,5.15,0,0,0-4.91-3.17,4.54,4.54,0,0,0-4.76,4.49c0,2.76,2.21,3.66,5,4.76s5.46,1.87,7.25,4.15A6.75,6.75,0,0,1,142,144c0,6.42-4.83,9-9.18,9a9.65,9.65,0,0,1-9.74-7.46l2.69-.83a7.19,7.19,0,0,0,7,5.66,6,6,0,0,0,6.28-6.21c0-3.18-2-4.36-6.28-6.08-2.84-1.18-4.91-2.08-6.15-3.73a5.94,5.94,0,0,1-1.24-3.66c0-4.29,3.24-7.26,7.53-7.26a7.6,7.6,0,0,1,7.39,4.56Z" />
											<g id="Layer_2" data-name="Layer 2">
												<g id="Layer_1-2" data-name="Layer 1-2">
													<polygon class="cls-2"
														points="57.92 0 57.92 49.23 5.79 49.23 5.79 0 0 0 0 104.26 5.79 104.26 5.79 55.02 57.92 55.02 57.92 104.26 63.71 104.26 63.71 0 57.92 0" />
													<path class="cls-2"
														d="M277.06,34.75A35.68,35.68,0,0,0,247.13,51V40.54h-5.79v86.89h5.79V89.91a35.7,35.7,0,1,0,29.93-55.16Zm0,65.65A29.93,29.93,0,1,1,307,70.47,29.93,29.93,0,0,1,277.06,100.4Z" />
													<path class="cls-2"
														d="M194,34.75A35.68,35.68,0,0,0,164.11,51V40.54h-5.79v86.89h5.79V89.91A35.7,35.7,0,1,0,194,34.75Zm0,65.65A29.93,29.93,0,1,1,224,70.47,29.93,29.93,0,0,1,194,100.4Z" />
													<path class="cls-2"
														d="M73.37,70.47A35.69,35.69,0,0,0,139,89.91v16.28h5.79V40.54H139V51A35.7,35.7,0,0,0,73.37,70.47Zm5.79,0a29.93,29.93,0,1,1,29.92,29.93A29.93,29.93,0,0,1,79.16,70.47Z" />
												</g>
											</g>
											<path class="cls-2"
												d="M364.9,40.54l-20.33,42.4-20.21-42.4h-5.79l22.78,49.12-18.21,38c.27-1.34-74.32,154.11-168.27,153.3-82.18-.71-105.74-76.69-106.19-88.81-.38-10.3-20.58-15.48-5.79,25.1,14.41,30.8,57.64,73.64,112,71.24C217.53,293.22,292,205,328.35,130.17l42.34-89.63Z" />
											<path class="cls-3"
												d="M153.9,214.33s-7.72-8.94-10.61-8.94-4.8,2.74-8.68,2.74-9.76-7.27-21.3-7.27-34.7,3.82-34.7,34.71,35.84,38.41,40.12,39.48,19.73-4.73,31.31-4.73,17.38,5.8,29,7.73,71.44-29,81.09-54.06-13.51-34.75-21.24-36.69-16.86-1.11-21.94,0-22.21,5.23-18.6,3.87c7.07-2.68,2.61-17.58-11.58-19.31-3.84-.47-7.73,0-7.73,7.72" />
											<path class="cls-3"
												d="M155.83,187.3c-1.93,11.59,19.31,38.62,19.31,38.62l15.45-7.72h30.89" />
											<path class="cls-3"
												d="M180.93,193.1s-8.47,4.92-8.47,12.11,10.4,13,18.13,13" />
											<path class="cls-3" d="M175.14,225.92s-1.93,30.89,1.93,34.75" />
											<line class="cls-3" x1="126.87" y1="217.23" x2="126.87" y2="209.51" />
										</g>
									</g>
								</svg>
							</a>
							<?php if (!is_page_template('template-landing.php') && !is_page_template('template-form-submitted.php') && !is_page_template('template-lead-magnet.php')) : ?>
							<?php joints_top_nav(); ?>

							<?php else : ?>
							<?php joints_top_landing_nav(); ?>

							<?php endif; ?>
						</div>
					</div>
				</nav>
			</div>
		</header>
		<main>
			<?php get_template_part('modules/module', 'hero'); ?>