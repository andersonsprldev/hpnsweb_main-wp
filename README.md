# Anderson Master theme V1

## Requirements
wpand-theme requires [Node.js](https://nodejs.org) v6.9.x or newer. This doesn't mean you need to understand Node (or even Yarn) - it's just the steps we need to take to make sure all of our development tools are installed. 

## Getting Started 
### Download wpand-theme and install dependencies with yarn 
```bash
$ cd YOURSITE/wp-content/themes/wpand-theme
$ git clone git@bitbucket.org:andersonsprldev/wpand-theme.git
$ cd YOURSITE/wp-content/themes/wpand-theme/src/
$ yarn install (Make sure you have node 10 installed)
```

### Watching for Changes
```bash
$ yarn start
```