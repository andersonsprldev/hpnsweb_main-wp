<?php
/*
Template Name: Formations
*/
get_header(); ?>



<div id="myModal" class="modal">
    <div class="modal-content">
        <span class="close">&times;</span>
        <div class="content">
            <h2><?php _e('Je m\'inscris à la formation', 'wpand-theme'); ?></h2>
            <p><?php _e("Vous désirez vous inscrire à cette formation ? Envoyez-moi votre demande et je vous recontacte très rapidement.", 'wpand-theme'); ?></p>
        </div>
        <div class="popup form">

            <?php echo do_shortcode(get_field('form_shortcode_formation')); ?>
        </div>
    </div>
</div>
<section id="search-bar">
    <div class="container">
        <h2 class="title">Quelle formation recherchez-vous ?</h2>
        <ul class="no-list row between-md">
            <?php
            $terms = get_terms('training_type');

            foreach ($terms as $term) {
                wp_reset_query();
                $args = array(
                    'post_type' => 'solutions',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'training_type',
                            'field' => 'slug',
                            'terms' => array($term->slug)
                        ),
                    ),
                );
                $loop = new WP_Query($args);
                if ($loop->have_posts()) { ?>
                    <li class="item col-md-4">
                        <a href="#<?php echo $term->slug; ?>">
                            <button class="button secondary title title-formations <?php echo $term->slug; ?>">
                                <span class="title-button">
                                    <?php if ($term->slug == "coaching-individuel") {
                                        echo "Coaching";
                                    } else {
                                        echo "Formation";
                                    } ?>
                                </span>
                                <?php echo $term->name ?>
                            </button>
                        </a>
                    </li>
            <?php }
            }
            ?>
        </ul>
    </div>
</section>
<section id="formations">
    <div class="container">
        <ul class="list list-categories">
            <?php
            $terms = get_terms('training_type');

            foreach ($terms as $term) {
                wp_reset_query();
                $args = array(
                    'post_type' => 'solutions',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'training_type',
                            'field' => 'slug',
                            'terms' => array($term->slug),
                        ),
                    ),
                );

                $loop = new WP_Query($args);
                if ($loop->have_posts()) { ?>
                    <li class="item-category box" id="<?php echo $term->slug; ?>">
                        <h2 class="title title-consultations"><?php echo $term->name ?></h2>
                        <ul class="list-consultations no-list">
                            <?php
                            while ($loop->have_posts()) : $loop->the_post(); ?>
                                <li class="item-consultation row between-md">
                                    <div class="img-wrapper col-lg-4">
                                        <?php the_post_thumbnail('full'); ?>
                                    </div>
                                    <div class="content col-lg-7">
                                        <h2 class="title"><?php the_title(); ?></h2>
                                        <?php the_field('solution_description'); ?>
                                        <div class="cta-group row middle-xs">
                                        <?php if(get_field('formation_link_landing')) : ?>
                                                <a class="button primary" href="<?php the_field('url');?>">En savoir plus</a>
                                            <?php else : ?>
                                                <?php
                                                $link = get_field('formation_cta');
                                                if (!empty($link)) : ?>
                                                <a class='button primary' href='<?php echo $link['url']; ?>'><?php echo $link['title']; ?></a>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <div id="calendar" class="calendar">
                                        <div class="dates-wrapper">
                                            <?php
                                            if (have_rows('list_dates')) :
                                            ?>
                                                <ul class="list-dates no-list">
                                                    <h3>Dates de la formation</h3>
                                                    <?php
                                                    while (have_rows('list_dates')) : the_row(); ?>
                                                        <li class="item-date row between-xs <?php if (!get_sub_field('status')) {
                                                                                                echo "complete";
                                                                                            }; ?>">
                                                            <h4 data-name="<?php the_title(); ?>">
                                                                <span class="date"><?php the_sub_field('dates'); ?></span>
                                                                <span class="hour"><?php the_sub_field('duration'); ?></span>
                                                            </h4>
                                                            <?php if (!get_sub_field('status')) : ?>
                                                                <a class="button primary" href="">Complet !</a>
                                                            <?php else : ?>
                                                                <a class="button primary openModal" href="">Réserver</a>
                                                            <?php endif; ?>

                                                        </li>
                                                    <?php
                                                    endwhile; ?>
                                                </ul>
                                                <div class="cta-group">
                                                    <button class="load-more_dates">
                                                        + voir plus de dates
                                                    </button>
                                                </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    </li>
            <?php }
            }
            ?>
        </ul>
    </div>
</section>
<?php get_footer(); ?>
<script>
    jQuery(document).ready(function($) {
        var button;
        $('.openModal').click(function() {
            button = $(this);
        });
        var wpcf7ElmSolution = document.querySelector('.modal-content .wpcf7');
        if (wpcf7ElmSolution) {
            wpcf7ElmSolution.addEventListener('wpcf7mailsent', function(event) {
                $('#myModal').hide();
                setTimeout(() => {
                    button.text('Demande envoyée !');
                    $('.button.openModal').addClass('success');
                }, 500);

            }, false);
        }
    });
</script>