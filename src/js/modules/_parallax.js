
jQuery(document).ready(function ($) {
    $('body').on("mousemove", function (e) {
        let moveX = (e.pageX / $(this).width()) * 5 + 30;
        let moveY = (e.pageY / $(this).height()) * 5 + 30;
        $(this).find('.parallax').css("left", (moveX + "%"));
        $(this).find('.parallax').css("top", (moveY + "%"));
        // $('.solution-line__single span.wave').css("top",(moveY + "%"));
    })
});