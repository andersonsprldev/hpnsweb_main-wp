jQuery(document).ready(function ($) {

  // Swiper testimonials

  //initialize swiper when document ready
  var mySwiper = new Swiper('.swiper-container.swiper-testimonials', {
    // Optional parameters
    spaceBetween: 20,
    centeredSlides: true,
    // autoHeight: true,
    breakpoints: {
      // when window width is >= 480px
      480: {
        slidesPerView: 1
      },
      // when window width is >= 640px
      640: {
        slidesPerView: 2
      },
      992: {
        slidesPerView: 3
      }
    }
  });

    // Swiper testimonials landingh

  //initialize swiper when document ready
  var mySwiper = new Swiper('.swiper-container.swiper-testimonials-landing', {
    // Optional parameters
    spaceBetween: 20,
    centeredSlides: true,
    navigation: {
      nextEl: '.swiper-button-next.btn-next-testimonials-landing',
      prevEl: '.swiper-button-prev.btn-prev-testimonials-landing',
    },
    // autoHeight: true,
    breakpoints: {
      // when window width is >= 480px
      480: {
        slidesPerView: 1
      },
      // when window width is >= 640px
      640: {
        slidesPerView: 2
      },
      992: {
        slidesPerView: 3
      }
    },
    on: {
      slideChange: function () {
        $('.swiper-slide-active').prevAll().addClass('prev');
        var activeIndex = this.activeIndex;
        var realIndex = this.slides.eq(activeIndex);
        realIndex.removeClass('prev');
      },
    }
  });

  // swiper news

  var mySwiper = new Swiper('.swiper-container.swiper-news', {
    // Optional parameters
    spaceBetween: 20,
    // autoHeight: true,
    breakpoints: {
      // when window width is >= 480px
      480: {
        slidesPerView: 1,
      },
      768: {
        slidesPerView: 2
      },
      992: {
        slidesPerView: 3
      },
      1400: {
        slidesPerView: 4
      }
    },
    navigation: {
      nextEl: '.btn-next-news',
      prevEl: '.btn-prev-news',
    },
  });

  // swiper timeline

  var mySwiper = new Swiper('.swiper-container.swiper-timeline', {
    // Optional parameters
    spaceBetween: 10,
    centeredSlides: true,
    navigation: {
      nextEl: '.btn-next-timeline',
      prevEl: '.btn-prev-timeline',
    },
    breakpoints: {
      480: {
        slidesPerView: 1,
      },
      // when window width is >= 640px
      768: {
        spaceBetween: 60,
        slidesPerView: 2
      },
    },

  });

  var mySwiper = new Swiper('.swiper-container.swiper-gallery', {
    // Optional parameters
    spaceBetween: 10,
    navigation: {
      nextEl: '.btn-next-timeline',
      prevEl: '.btn-prev-timeline',
    },
    breakpoints: {
      480: {
        slidesPerView: 1,
      },
      // when window width is >= 640px
      768: {
        spaceBetween: 60,
        slidesPerView: 3
      },
    },

  });
});