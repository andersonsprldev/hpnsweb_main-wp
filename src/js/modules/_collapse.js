jQuery(document).ready(function ($) {

    $('.load-more_dates').each(function () {
        $(this).click(function () {
            var item = $(this).parent().siblings('.list-dates').find('li');

            item.toggleClass('expand');
            $(this).css('opacity', '0');
        });
    });

});