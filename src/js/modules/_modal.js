  //open contact modal

  var modal = document.getElementById("myModal");
  var openModal = document.querySelectorAll(".openModal");

  var span = document.getElementsByClassName("close")[0];
  if (openModal) {
    openModal.forEach(function (openModal) {
      openModal.onclick = function (e) {
        e.preventDefault();
        modal.style.display = "flex";
      }

      span.onclick = function () {
        modal.style.display = "none";
      }

      window.onclick = function (event) {
        if (event.target == modal) {
          modal.style.display = "none";
        }
      }
    });
  }