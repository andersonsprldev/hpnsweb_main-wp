// Mobile menu
jQuery(document).ready(function ($) {
        var dateInput = $("#form-input-date");
        var hiddenInput = $("#form-input-subject");
        var inputFormationName = $("#form-input-formation-name");

        $('.openModal').each(function () {
                $(this).click(function () {
                        var date = $(this).parent().find('h4 .date').html();
                        var title = $(this).parent().parent().siblings('.accordeon__item--title').find('h2').html();
                        var formationName = $(this).siblings('h4').attr('data-name');
                        dateInput.val(date);
                        hiddenInput.val(title);
                        inputFormationName.val(formationName);
                });
        });
});