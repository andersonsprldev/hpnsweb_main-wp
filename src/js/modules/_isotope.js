jQuery(document).ready(function ($) {
  // init Isotope
  var $grid = $('.solutions__wrapper').isotope({
    itemSelector: '.solution__single',
    layoutMode: 'fitRows',
    fitRows: {
      gutter: 25
    }
  });


  // filter items on button click

  $('.other-lines__list').on('click', '.other-lines__item', function () {
    var filterValue = $(this).attr('data-filter');
    $grid.isotope({ filter: filterValue });
  });


  //   Button to reset isotope

  // $(".isotope-reset").click(function (e) {
  //   e.preventDefault();
  //   $grid.isotope({
  //     filter: '*'
  //   });
  // });


  // use value of search field to filter
  // var $quicksearch = $('.quicksearch').keyup(debounce(function () {
  //   qsRegex = new RegExp($quicksearch.val(), 'gi');
  //   $grid.isotope({
  //     filter: function () {
  //       return qsRegex ? $(this).text().match(qsRegex) : true;
  //     }
  //   });
  // }, 200));

});