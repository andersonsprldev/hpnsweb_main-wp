
// sticky element on scroll

jQuery(document).ready(function ($) {

    var stickyWrapper = $('.sticky__wrapper');

    if (stickyWrapper.length) {
        var topWrapper = stickyWrapper.offset().top;

        $(window).scroll(function () {
            if ($(window).scrollTop() > topWrapper + 99) {
                stickyWrapper.addClass('sticky-fixed');
            } else {
                stickyWrapper.removeClass('sticky-fixed');
            }
        });
    }

});