// Accordeon

jQuery(document).ready(function ($) {
  $('.accordeon__item--title').click(function () {
    var item = $(this).parent().find('.accordeon__item--content')
    item.toggleClass('expanded');
    $(this).find('.expand').toggleClass('rotate');
    item.parent().parent().siblings().find('.expanded').removeClass('expanded');
    item.parent().parent().siblings().find('.expand').removeClass('rotate');
  });

});