jQuery(document).ready(function ($) {
  // Smooth scroll to section 
var navHeight = $('#main__nav').height();
  $(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();

    $('html, body').animate({
      scrollTop: $($.attr(this, 'href')).offset().top - navHeight - 10
    }, 500);
  });
});