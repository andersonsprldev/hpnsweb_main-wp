jQuery(document).ready(function($) {


    // header height for smooth scrolls
  
    var headerHeight = $('header').height();
  
    // Header scroll detect
  
    var lastScrollTop = 0;
    $(window).scroll(function (event) {
      var st = $(this).scrollTop();
  
      // For iOS bouncing issue 
  
      // if ((st > lastScrollTop && lastScrollTop >= 0 && st > 0)) {
      //   $('header, #secondary__nav').addClass("scrollDown");
      //   $('header, #secondary__nav').removeClass("scrollUp");
        
      // } else {
      //   $('header, #secondary__nav').removeClass("scrollDown");
      //   $('header, #secondary__nav').addClass("scrollUp");
        
      // }
  
      if (st < 50) {
        $('header, nav, .burger__menu').addClass("scrollTop");
        $('header, #secondary__nav').removeClass("scrolled");
        
      } else {
        $('header, nav, .burger__menu').removeClass("scrollTop");
        $('header').addClass("scrolled");
      }
      lastScrollTop = st;
    });
  
  });
  
  

  
