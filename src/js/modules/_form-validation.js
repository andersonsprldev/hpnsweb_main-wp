jQuery(document).ready(function ($) {
    var v = $(".wpcf7-form").validate({
        focusInvalid: true,
        rules: {
            your_name: {
                required: true
            },
            your_email: {
                required: true,
                email: true
            },
            your_job: {
                required: true
            },
            your_date: {
                required: true
            }
        },
        errorElement: "span",
        errorClass: "error",
        errorPlacement: function (error, element) {
            error.addClass(element);
            $('input.error:first').focus();
        }
    });
});