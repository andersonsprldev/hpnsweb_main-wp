// Urgent bar on bottom of pages

jQuery(document).ready(function ($) {
    var urgentBar = $('#bottom-bar, #bottom-bar__landing');
    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() == $(document).height()) {
            urgentBar.addClass('hide');
        } else {
            urgentBar.removeClass('hide');

        }
    });

    $(window).resize(function () {
        if (window.matchMedia('(max-width: 767px)').matches) {
            $('.avatar').click(function(){
                urgentBar.toggleClass('active');
            })
        } 
    }).resize();

    // Close on click 
    $('#bottom-bar .close').click(function(){
        $('#bottom-bar').hide();
    });
});