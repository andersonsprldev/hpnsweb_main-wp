// Urgent bar on bottom of pages

jQuery(document).ready(function ($) {

    var fixedButton = $('.button.side');

    $(window).scroll(function () {

        if ($(this).scrollTop() > 120) {
            fixedButton.addClass('expand');
        } else {
            fixedButton.removeClass('expand');
        }
    });

});