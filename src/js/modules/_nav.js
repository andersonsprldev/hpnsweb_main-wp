// Mobile menu
jQuery(document).ready(function ($) {
  $('.burger__menu').click(function (e) {
    e.preventDefault();
    $(this).toggleClass("open");
    $('header, nav, .logo svg').toggleClass("open-menu");
  });
});