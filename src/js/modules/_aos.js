jQuery(document).ready(function ($) {

  $('section:nth-child(2)').attr('data-aos', 'fade-up');
  $('section:nth-child(2)').attr('data-aos-delay', '300');



  AOS.init( {
    disable: false, // accepts following values: 'phone', 'tablet', 'mobile', boolean, expression or function
  startEvent: 'DOMContentLoaded', // name of the event dispatched on the document, that AOS should initialize on
  initClassName: 'aos-init', // class applied after initialization
  animatedClassName: 'aos-animate', // class applied on animation
  useClassNames: false, // if true, will add content of `data-aos` as classes on scroll
  // disableMutationObserver: false, // disables automatic mutations' detections (advanced)
  // debounceDelay: 50, // the delay on debounce used while resizing window (advanced)
  // throttleDelay: 99, // the delay on throttle used while scrolling the page (advanced)
  });

  // AOS delay

  // var delay = 0;
  // $('.topic-variation__title').each(function () {
  //   delay = delay + 20;
  //   $(this).attr('data-aos-delay', delay);
  // });

  // $('.topic-variation__wrapper:odd').each(function () {
  //   $(this).find('.topic-variation__title').attr('data-aos', 'fade-left');
  // });


  // $('.topic-variation__wrapper:odd').each(function () {
  //   $(this).find('.topic-variation__content').attr('data-aos', 'fade-right');
  // });

  // $('.topic-variation__content').each(function () {
  //   delay = delay + 20;
  //   $(this).attr('data-aos-delay', delay);
  // });
});