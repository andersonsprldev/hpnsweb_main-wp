// Import Foundation
// import 'script-loader!what-input/dist/what-input';

// Import the apps styles
import sass from '../scss/main.scss'

// Import our modules
import './modules/_header.js'
import './modules/_modal.js'
// import './modules/_isotope.js'
import './modules/_aos.js'
import './modules/_accordeon.js'
import './modules/_nav.js'
import './modules/_hide-wpbar.js'
import './modules/_swiper.js'
import './modules/_form-validation.js'
import './modules/_sticky-on-scroll.js'
// import './modules/_follow-on-scroll.js'
import './modules/_anchor.js'
import './modules/_maps.js'
import './modules/_prefilled-field-form.js'
import './modules/_parallax.js'
import './modules/_urgent-bar.js'
import './modules/_fixed-button.js'
import './modules/_collapse.js'
import './modules/_masonry.js'
// import './modules/_issue-collector.js'
