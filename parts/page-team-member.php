<!-- It shows specific solutions linked to Solution line page -->

<?php


$loop = new WP_Query(array(
    'post_type' => 'team'
));

if ($loop->have_posts()) :
    while ($loop->have_posts()) : $loop->the_post();
?>
        <div class="team__single generic-box np">
            <div class="img__wrapper">
            <?php if (get_field('member_image')) :
                $image = get_field('member_image'); ?>
                <?php echo wp_get_attachment_image($image, 'full'); ?>
            <?php endif; ?>
                
            </div>
            <div class="team__content">
                <h3><?php the_field('member_firstname'); ?> <?php the_field('member_lastname'); ?></h3>
                <small><?php the_field('member_function'); ?></small>
                <p><?php the_field('member_quote'); ?></p>
            </div>
        </div>
    <?php
    endwhile;
else :
    ?>
    <p>No articles found</p>
<?php
endif;
wp_reset_query();
?>