<!-- It shows specific solutions linked to Solution line page -->

<?php


$loop = new WP_Query(array(
    'post_type' => 'jobs'
));

if ($loop->have_posts()) :
    while ($loop->have_posts()) : $loop->the_post();
?>
        <div class="job__single generic-box">
            <div class="img__wrapper">
                <?php the_post_thumbnail('full'); ?>
            </div>
            <div class="job__content">
                <h2><?php the_title(); ?></h2>
                <?php $excerpt = wp_trim_words( get_field('job_description' ), $num_words = 14, $more = '...' ); ?>
                <p><?php echo $excerpt; ?></p>
                <a class="button primary" href="<?php the_permalink(); ?>">read more</a>
            </div>
        </div>
    <?php
    endwhile;
else :
    ?>
    <p>No articles found</p>
<?php
endif;
wp_reset_query();
?>