<?php

/**
 * The template for displaying all single posts and attachments
 */

get_header(); ?>

<div class="content">

    <main role="main">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

                    <header class="content__section">
                        <div class="content__header container">
                            <h1 class="title__page col-lg-6"><?php the_title(); ?></h1>
                            <h3><?php the_field('job_address'); ?></h3>
                            <a href="/jobs/" class="back"><?php _e('Back to all jobs', 'wpand-theme'); ?></a>
                        </div>
                    </header>

                    <section class="description__wrapper container">
                        <div class="content__section">
                            <div class="col-lg-8">
                                <h2><?php _e('Description', 'wpand-theme'); ?></h2>
                                <?php the_field('job_description'); ?>
                            </div>
                        </div>
                    </section>
                    <section class="responsabilities__wrapper container">
                        <div class="content__section">
                            <div class="col-lg-8">
                                <h2><?php _e('Responsabilities', 'wpand-theme'); ?></h2>
                                <?php the_field('job_responsabilities'); ?>
                            </div>
                        </div>
                    </section>
                    <section id="form">
                        <div class="container">
                            <div class="content__section">
                                <h2 class="aligncenter"><?php _e('Are you interested ?', 'wpand-theme'); ?></h2>
                                <h3 class="aligncenter"><?php _e('Please fill this form and we will contact you soon', 'wpand-theme'); ?></h3>
                                <div class="content__section">
                                    <?php echo do_shortcode('[contact-form-7 id="512" title="Contact form 1"]'); ?>
                                </div>
                            </div>
                        </div>
                    </section>
                </article>

            <?php endwhile;
        else : ?>

            <?php get_template_part('parts/content', 'missing'); ?>

        <?php endif; ?>

    </main> <!-- end #main -->

</div> <!-- end #content -->

<?php get_footer(); ?>